experiences = [
    {
        'name': 'Mạo hiểm',
        'icon': 'trip_origin'
    },
    {
        'name': 'Biển',
        'icon': 'beach_access'
    },
    {
        'name': 'Văn hóa',
        'icon': 'public'
    },
    {
        'name': 'Giải trí',
        'icon': 'toys'
    },
    {
        'name': 'Lịch sử',
        'icon': 'account_balance'
    },
    {
        'name': 'Trăng mật',
        'icon': 'favorite'
    },
    {
        'name': 'Nghỉ dưỡng',
        'icon': 'airline_seat_flat'
    },
    {
        'name': 'Mua sắm',
        'icon': 'shopping_cart'
    },
    {
        'name': 'Ăn uống',
        'icon': 'restaurant'
    },
    {
        'name': 'Khoa học',
        'icon': 'language'
    }
]

it_categories = [
    {
        'name': 'Gia đình',
        'icon': 'home'
    },
    {
        'name': 'Cặp đôi',
        'icon': 'favorite'
    },
    {
        'name': 'Nhóm',
        'icon': 'group'
    }
]

event_categories = [
    dict(
        name='Ăn uống',
        code='cuisine',
        translations=[
            dict(locale='vi', name='Ăn uống'),
            dict(locale='en', name='Cuisine'),
        ],
        icon='local_dining',
        color='orange',
        items=[
            dict(
                name='Bữa sáng',
                code='break-fast',
                translations=[
                    dict(locale='vi', name='Bữa sáng'),
                    dict(locale='en', name='Breakfast'),
                ],
            ),
            dict(
                name='Bữa trưa',
                code='lunch',
                translations=[
                    dict(locale='vi', name='Bữa trưa'),
                    dict(locale='en', name='Lunch'),
                ],
            ),
            dict(
                name='Bữa tối',
                code='dinner',
                translations=[
                    dict(locale='vi', name='Bữa tối'),
                    dict(locale='en', name='Dinner'),
                ],
            ),
            dict(
                name='Bữa phụ',
                code='snack',
                translations=[
                    dict(locale='vi', name='Bữa phụ'),
                    dict(locale='en', name='Snack'),
                ],
            )
        ],
        place_types=['bakery', 'restaurant'],
        meta_fields=[]
    ),
    dict(
        name='Chỗ ở',
        code='stay',
        translations=[
            dict(locale='vi', name='Chỗ ở'),
            dict(locale='en', name='Accommodation'),
        ],
        color='indigo',
        icon='hotel',
        items=[
            dict(
                name='Check in',
                code='check-in',
                translations=[
                    dict(locale='vi', name='Checkin'),
                    dict(locale='en', name='Checkin'),
                ],
            ),
            dict(
                name='Check out',
                code='check-out',
                translations=[
                    dict(locale='vi', name='Checkout'),
                    dict(locale='en', name='Checkout'),
                ],
            )
        ],
        meta_fields=[
            dict(
                label='Room Type',
                id='room_type',
                placeholder='e.g, Deluxe'
            )
        ]
    ),
    dict(
        code='travel',
        name='Di chuyển',
        translations=[
            dict(locale='vi', name='Di chuyển'),
            dict(locale='en', name='Transportation'),
        ],
        color='primary',
        items=[
            dict(
                name='Máy bay',
                code='flight',
                meta_fields=[
                    dict(
                        label='Hãng',
                        id='brand',
                        placeholder='e.g, Vietnam Airline'
                    ),
                    dict(
                        label='Mã chuyến bay',
                        id='flight_number',
                        placeholder='e.g, VN5142'
                    ),
                    dict(
                        label='Mã đặt chỗ',
                        id='booking_number',
                        placeholder='e.g, ASD12345'
                    )
                ],
                translations=[
                    dict(locale='vi', name='Máy bay'),
                    dict(locale='en', name='Flight'),
                ],
            ),
            dict(
                name='Tàu hỏa',
                code='train',
                translations=[
                    dict(locale='vi', name='Tàu hỏa'),
                    dict(locale='en', name='Train'),
                ],
            ),
            dict(
                name='Ô tô',
                code='car',
                translations=[
                    dict(locale='vi', name='Oto'),
                    dict(locale='en', name='Car'),
                ],
            ),
            dict(
                name='Xe khách',
                code='coach',
                translations=[
                    dict(locale='vi', name='Xe khách'),
                    dict(locale='en', name='Coach'),
                ],
            ),
            dict(
                name='Xe máy',
                code='motorbike',
                translations=[
                    dict(locale='vi', name='Xe máy'),
                    dict(locale='en', name='Motorbike'),
                ],
            ),
            dict(
                name='Xe đạp',
                code='bicycle',
                translations=[
                    dict(locale='vi', name='Xe đạp'),
                    dict(locale='en', name='Bicycle'),
                ],
            ),
            dict(
                name='Đi bộ',
                code='walk',
                translations=[
                    dict(locale='vi', name='Đi bộ'),
                    dict(locale='en', name='Walk'),
                ],
            ),
            dict(
                name='Phương tiện khác',
                code='others',
                translations=[
                    dict(locale='vi', name='Phương tiện khác'),
                    dict(locale='en', name='Others'),
                ],
            )
        ],
        icon='transfer_within_a_station'
    ),
    dict(
        name='Hoạt động',
        code='activity',
        translations=[
            dict(locale='vi', name='Hoạt động'),
            dict(locale='en', name='Activity'),
        ],
        color='red',
        items=[
            dict(
                name='Xem phim',
                code='movie',
                translations=[
                    dict(locale='vi', name='Xem phim'),
                    dict(locale='en', name='Cinema'),
                ],
            ),
            dict(
                name='Tham quan',
                code='sight-seeing',
                translations=[
                    dict(locale='vi', name='Tham quan'),
                    dict(locale='en', name='Sight seeing'),
                ],
            ),
            dict(
                name='Công viên',
                code='park',
                translations=[
                    dict(locale='vi', name='Công viên'),
                    dict(locale='en', name='Park'),
                ],
            ),
            dict(
                name='Dạo phố',
                code='street-walk',
                translations=[
                    dict(locale='vi', name='Dạo phố'),
                    dict(locale='en', name='Street view'),
                ],
            ),
            dict(
                name='Bar/Club',
                code='bar',
                translations=[
                    dict(locale='vi', name='Bar/Club'),
                    dict(locale='en', name='Bar/Club'),
                ],
            ),
            dict(
                name='Team building',
                code='team-building',
                translations=[
                    dict(locale='vi', name='Team building'),
                    dict(locale='en', name='Team building'),
                ],
            )
        ],
        icon='local_play',
        meta_fields=[]
    )
]

test_user = {
    "name": "test-{}",
    "email": "test_{}@gmail.com"
}

notification_action_types = [
    dict(
        code='comment',
        content_template='{} đã bình luận về {} của {}'
    ),
    dict(
        code='reply-comment',
        content_template='{} đã trả lời bình luận {} về {}'
    )
]

locales = [
    dict(code='vi', name='Tiếng việt'),
    dict(code='en', name='English'),
]

#
# items = [
#     {
#         'name': 'Máy Bay',
#         'icon': 'local_airport',
#         'meta_fields': [
#             dict(
#                 id='flight_number',
#                 label='Flight Number',
#                 icon='flight',
#                 placeholder='e.g, VN123'
#             ),
#             dict(
#                 id='terminal',
#                 label='Terminal',
#                 placeholder='e.g, 4',
#                 icon='vignette'
#             ),
#             dict(
#                 id='gate',
#                 label='Gate',
#                 icon='panorama_vertical',
#                 placeholder='e,g 15'
#             )
#         ]
#     },
#     {
#         'name': 'Tàu Hỏa',
#         'icon': 'directions_railway',
#         'meta_fields': [
#             dict(
#                 id='train_number',
#                 label='Train Number',
#                 icon='train',
#                 placeholder='e.g, TN6',
#             ),
#             dict(
#                 id='couch',
#                 label='Couch',
#                 placeholder='e.g A3',
#                 icon='panorama_horizontal'
#             )
#         ]
#     },
#     {
#         'name': 'Xe Khách',
#         'icon': 'directions_bus',
#         'meta_fields': [
#             dict(
#                 id='vehicle_number',
#                 label='Vehicle Number',
#                 icon='directions_bus',
#                 placeholder='e.g, B14',
#             )
#         ]
#     },
#     {
#         'name': 'Tàu Thủy',
#         'icon': 'directions_boat',
#         'meta_fields': [
#             dict(
#                 id='cabin_type',
#                 label='Cabin Type',
#                 placeholder='e.g, Deluxe',
#                 icon='category'
#             ),
#             dict(
#                 id='cabin_number',
#                 label='Cabin Number',
#                 placeholder='e.g, 514',
#                 icon='layers'
#             )
#         ]
#     },
#     {
#         'name': 'Khác',
#         'icon': 'grain',
#         'meta_fields': []
#     }
# ]
