from slugify import slugify

from tripary.databases.postgres import *
from config import BaseConfig

from .data import event_categories, test_user, notification_action_types, locales


class Seeder(object):
    def __init__(self, postgres_db):
        self.postgres_db = postgres_db
        self.session = self.postgres_db.start_session()

    def init_db(self):
        self.event_category()
        if BaseConfig.ENV != 'production':
            self.user()

    def drop_db(self):
        confirm = input('Are u fucking sure? (y/n): ')
        if confirm.lower() != 'y':
            return
        BaseModel.metadata.drop_all(self.postgres_db.engine)

    def reset_db(self, create_all=False):
        self.drop_db()
        if create_all:
            BaseModel.metadata.create_all(self.postgres_db.engine)

        self.init_db()

    def refresh_session(self):
        self.session.close()
        self.session = self.postgres_db.start_session()
        return self.session

    def event_category(self):
        for c in event_categories:
            c_code = c['code']
            if self.session.query(EventCategory).filter(
                    EventCategory.code == c_code
            ).first():
                continue

            cate = EventCategory()
            cate.name = c['name']
            cate.slug = slugify(c['name'])
            cate.code = c_code
            cate.icon = c['icon']
            cate.color = c['color']

            place_types = c.get('place_types')
            if place_types:
                cate.place_types = place_types
            meta_fields = c.get('meta_fields')
            if meta_fields:
                cate.meta_fields = meta_fields

            self.session.add(cate)
            for c_i in c['items']:
                c_i_code = c_i['code']
                cate_item = EventSubCategory()
                cate_item.code = c_i_code
                cate_item.name = c_i['name']
                cate_item.slug = slugify(c_i['name'])
                c_i_icon = c_i.get('icon')
                c_i_meta_fields = c_i.get('meta_fields')
                c_i_place_types = c_i.get('c_i_place_types')
                if c_i_icon:
                    cate_item.icon = c_i_icon
                if c_i_meta_fields:
                    cate_item.meta_fields = c_i_meta_fields
                if c_i_place_types:
                    cate_item.place_types = c_i_place_types
                cate_item.category = cate
                self.session.add(cate_item)
        self.session.commit()

    def notification_action_type(self):
        for item_data in notification_action_types:
            code = item_data['code']
            if self.session.query(NotificationActionType).filter(
                    NotificationActionType.code == code
            ).first():
                continue
            action_type = NotificationActionType()
            action_type.code = code
            action_type.content_template = item_data['content_template']
            self.session.add(action_type)
        self.session.commit()

    def user(self):
        for i in range(50):
            u = User()
            u.name = test_user['name'].format(i)
            u.slug = slugify(u.name)

            if self.session.query(User).filter(
                    User.slug == u.slug
            ).first():
                continue
            self.session.add(u)
        self.session.commit()

    def locale(self):
        for locale_data in locales:
            code = locale_data['code']
            if self.session.query(Locale).filter(Locale.code == code).first():
                continue
            locale = Locale()
            locale.code = code
            locale.name = locale_data['name']
            locale.slug = slugify(locale.name)
            self.session.add(locale)
        self.session.commit()

    def event_category_translation(self):
        for category_data in event_categories:
            c_code = category_data['code']
            cate = self.session.query(EventCategory).filter_by(code=c_code).first()
            if not cate:
                continue

            translations = category_data['translations']
            for tran_data in translations:
                locale = tran_data['locale']
                if self.session.query(EventCategoryTranslation).filter(
                        EventCategoryTranslation.locale == locale,
                        EventCategoryTranslation.category_id == cate.id
                ).first():
                    continue
                translation = EventCategoryTranslation()
                translation.locale = locale
                translation.name = tran_data['name']
                translation.slug = translation.name
                translation.category = cate
                self.session.add(translation)
            s_cs = category_data['items']
            for s_c_data in s_cs:
                s_cate = self.session.query(EventSubCategory).filter(
                    EventSubCategory.code == s_c_data['code'],
                    EventSubCategory.category_id == cate.id
                ).first()
                if not s_cate:
                    continue

                translations = s_c_data['translations']
                for tran_data in translations:
                    locale = tran_data['locale']
                    if self.session.query(EventSubCategoryTranslation).filter(
                            EventSubCategoryTranslation.locale == locale,
                            EventSubCategoryTranslation.sub_category_id == s_cate.id
                    ).first():
                        continue
                    translation = EventSubCategoryTranslation()
                    translation.locale = locale
                    translation.name = tran_data['name']
                    translation.slug = translation.name
                    translation.sub_category = s_cate
                    self.session.add(translation)

        self.session.commit()
