import requests
import os

from tripary.databases.postgres import *
from tripary.common.constants import APP_ROOT_DIR
from tripary.clients import google_storage_client


class MigrationWorker(object):
    def __init__(self, session):
        self.session = session

    def move_storage(self):
        tmp_dir = os.path.join(APP_ROOT_DIR, 'tmp')

        def run(file, file_type):
            try:
                res = requests.get(file.url)
            except Exception as e:
                print(e)
                return run(file, file_type)

            if res.status_code != 200:
                print(res.status_code)
                return run(file, file_type)

            tmp_path = os.path.join(tmp_dir, file.id)
            with open(tmp_path, 'wb') as f:
                f.write(res.content)

            try:
                upload_data = google_storage_client.upload(file_type=file_type,
                                                           file_name=file.name,
                                                           file_path=tmp_path,
                                                           path=[file.object_type, file.object_id])
            except Exception as e:
                print(e)
                return run(file, file_type)

            file.path = upload_data['path']
            file.url = upload_data['url']
            self.session.add(file)
            self.session.commit()
            os.remove(tmp_path)

        images = self.session.query(Image)
        total = images.count()
        current = 1
        for i in self.session.query(Image):
            run(i, 'image')
            print('Moving image: %s/%s' % (current, total))
            current += 1

        attachments = self.session.query(Attachment)
        total = attachments.count()
        current = 1
        for a in self.session.query(Attachment):
            run(a, 'attachment')
            print('Moving attachment: %s/%s' % (current, total))
            current += 1
