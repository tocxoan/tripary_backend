"""add desc column to image table

Revision ID: 7d61d9a150b7
Revises: 5a8f10c366c7
Create Date: 2019-06-10 09:51:52.614951

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7d61d9a150b7'
down_revision = '5a8f10c366c7'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('image', sa.Column('desc', sa.String(length=3000), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_unique_constraint('notification_action_type_code_key', 'notification_action_type', ['code'])
    # ### end Alembic commands ###
