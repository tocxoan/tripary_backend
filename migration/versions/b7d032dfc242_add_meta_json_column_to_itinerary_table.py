"""add meta JSON column to itinerary table

Revision ID: b7d032dfc242
Revises: aad644de1f89
Create Date: 2019-06-18 15:07:41.843682

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'b7d032dfc242'
down_revision = 'aad644de1f89'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('itinerary', sa.Column('meta', postgresql.JSONB(astext_type=sa.Text()), nullable=True))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('itinerary', 'meta')
    # ### end Alembic commands ###
