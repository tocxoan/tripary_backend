"""remove notification

Revision ID: d39dcc9013c2
Revises: b8546111103e
Create Date: 2019-06-05 10:58:35.180415

"""
from alembic import op
import sqlalchemy as sa
from sqlalchemy.dialects import postgresql

# revision identifiers, used by Alembic.
revision = 'd39dcc9013c2'
down_revision = 'b8546111103e'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###

    op.drop_index('ix_notification_event_creator_created_at', table_name='notification_event_creator')
    op.drop_index('ix_notification_event_creator_creator_id', table_name='notification_event_creator')
    op.drop_index('ix_notification_event_creator_creator_type', table_name='notification_event_creator')
    op.drop_index('ix_notification_event_creator_event_time', table_name='notification_event_creator')
    op.drop_index('ix_notification_event_creator_notification_id', table_name='notification_event_creator')
    op.drop_index('ix_notification_event_creator_slug', table_name='notification_event_creator')
    op.drop_index('ix_notification_event_creator_updated_at', table_name='notification_event_creator')
    op.drop_table('notification_event_creator')

    op.drop_index('ix_notification_action_type_id', table_name='notification')
    op.drop_index('ix_notification_created_at', table_name='notification')
    op.drop_index('ix_notification_event_time', table_name='notification')
    op.drop_index('ix_notification_object_id', table_name='notification')
    op.drop_index('ix_notification_object_type', table_name='notification')
    op.drop_index('ix_notification_read_at', table_name='notification')
    op.drop_index('ix_notification_seen_at', table_name='notification')
    op.drop_index('ix_notification_sent_at', table_name='notification')
    op.drop_index('ix_notification_slug', table_name='notification')
    op.drop_index('ix_notification_status', table_name='notification')
    op.drop_index('ix_notification_updated_at', table_name='notification')
    op.drop_index('ix_notification_user_id', table_name='notification')
    op.drop_table('notification')

    op.drop_index('ix_notification_action_type_code', table_name='notification_action_type')
    op.drop_index('ix_notification_action_type_created_at', table_name='notification_action_type')
    op.drop_index('ix_notification_action_type_slug', table_name='notification_action_type')
    op.drop_index('ix_notification_action_type_updated_at', table_name='notification_action_type')
    op.drop_table('notification_action_type')

    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('notification_event_creator',
    sa.Column('id', sa.VARCHAR(length=36), autoincrement=False, nullable=False),
    sa.Column('slug', sa.VARCHAR(length=500), autoincrement=False, nullable=True),
    sa.Column('created_at', postgresql.TIMESTAMP(), server_default=sa.text("timezone('utc'::text, now())"), autoincrement=False, nullable=True),
    sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('deleted', sa.BOOLEAN(), autoincrement=False, nullable=True),
    sa.Column('deletable', sa.BOOLEAN(), autoincrement=False, nullable=True),
    sa.Column('editable', sa.BOOLEAN(), autoincrement=False, nullable=True),
    sa.Column('creator_type', sa.VARCHAR(length=30), autoincrement=False, nullable=False),
    sa.Column('creator_id', sa.VARCHAR(length=36), autoincrement=False, nullable=False),
    sa.Column('notification_id', sa.VARCHAR(length=36), autoincrement=False, nullable=False),
    sa.Column('event_time', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.ForeignKeyConstraint(['notification_id'], ['notification.id'], name='notification_event_creator_notification_id_fkey'),
    sa.PrimaryKeyConstraint('id', name='notification_event_creator_pkey')
    )
    op.create_index('ix_notification_event_creator_updated_at', 'notification_event_creator', ['updated_at'], unique=False)
    op.create_index('ix_notification_event_creator_slug', 'notification_event_creator', ['slug'], unique=False)
    op.create_index('ix_notification_event_creator_notification_id', 'notification_event_creator', ['notification_id'], unique=False)
    op.create_index('ix_notification_event_creator_event_time', 'notification_event_creator', ['event_time'], unique=False)
    op.create_index('ix_notification_event_creator_creator_type', 'notification_event_creator', ['creator_type'], unique=False)
    op.create_index('ix_notification_event_creator_creator_id', 'notification_event_creator', ['creator_id'], unique=False)
    op.create_index('ix_notification_event_creator_created_at', 'notification_event_creator', ['created_at'], unique=False)
    op.create_table('notification',
    sa.Column('id', sa.VARCHAR(length=36), autoincrement=False, nullable=False),
    sa.Column('slug', sa.VARCHAR(length=500), autoincrement=False, nullable=True),
    sa.Column('created_at', postgresql.TIMESTAMP(), server_default=sa.text("timezone('utc'::text, now())"), autoincrement=False, nullable=True),
    sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('deleted', sa.BOOLEAN(), autoincrement=False, nullable=True),
    sa.Column('deletable', sa.BOOLEAN(), autoincrement=False, nullable=True),
    sa.Column('editable', sa.BOOLEAN(), autoincrement=False, nullable=True),
    sa.Column('user_id', sa.VARCHAR(length=36), autoincrement=False, nullable=False),
    sa.Column('status', sa.VARCHAR(length=30), autoincrement=False, nullable=False),
    sa.Column('data', postgresql.JSONB(astext_type=sa.Text()), autoincrement=False, nullable=True),
    sa.Column('sent_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('seen_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('read_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('event_time', postgresql.TIMESTAMP(), autoincrement=False, nullable=False),
    sa.Column('avatar_url', sa.VARCHAR(length=500), autoincrement=False, nullable=True),
    sa.Column('action_type_id', sa.VARCHAR(length=36), autoincrement=False, nullable=False),
    sa.Column('object_id', sa.VARCHAR(length=36), autoincrement=False, nullable=False),
    sa.Column('object_type', sa.VARCHAR(length=30), autoincrement=False, nullable=False),
    sa.ForeignKeyConstraint(['action_type_id'], ['notification_action_type.id'], name='notification_action_type_id_fkey'),
    sa.ForeignKeyConstraint(['user_id'], ['user.id'], name='notification_user_id_fkey'),
    sa.PrimaryKeyConstraint('id', name='notification_pkey')
    )
    op.create_index('ix_notification_user_id', 'notification', ['user_id'], unique=False)
    op.create_index('ix_notification_updated_at', 'notification', ['updated_at'], unique=False)
    op.create_index('ix_notification_status', 'notification', ['status'], unique=False)
    op.create_index('ix_notification_slug', 'notification', ['slug'], unique=False)
    op.create_index('ix_notification_sent_at', 'notification', ['sent_at'], unique=False)
    op.create_index('ix_notification_seen_at', 'notification', ['seen_at'], unique=False)
    op.create_index('ix_notification_read_at', 'notification', ['read_at'], unique=False)
    op.create_index('ix_notification_object_type', 'notification', ['object_type'], unique=False)
    op.create_index('ix_notification_object_id', 'notification', ['object_id'], unique=False)
    op.create_index('ix_notification_event_time', 'notification', ['event_time'], unique=False)
    op.create_index('ix_notification_created_at', 'notification', ['created_at'], unique=False)
    op.create_index('ix_notification_action_type_id', 'notification', ['action_type_id'], unique=False)
    op.create_table('notification_action_type',
    sa.Column('id', sa.VARCHAR(length=36), autoincrement=False, nullable=False),
    sa.Column('slug', sa.VARCHAR(length=500), autoincrement=False, nullable=True),
    sa.Column('created_at', postgresql.TIMESTAMP(), server_default=sa.text("timezone('utc'::text, now())"), autoincrement=False, nullable=True),
    sa.Column('updated_at', postgresql.TIMESTAMP(), autoincrement=False, nullable=True),
    sa.Column('deleted', sa.BOOLEAN(), autoincrement=False, nullable=True),
    sa.Column('deletable', sa.BOOLEAN(), autoincrement=False, nullable=True),
    sa.Column('editable', sa.BOOLEAN(), autoincrement=False, nullable=True),
    sa.Column('code', sa.VARCHAR(length=30), autoincrement=False, nullable=False),
    sa.Column('content_template', sa.VARCHAR(length=3000), autoincrement=False, nullable=False),
    sa.PrimaryKeyConstraint('id', name='notification_action_type_pkey')
    )
    op.create_index('ix_notification_action_type_updated_at', 'notification_action_type', ['updated_at'], unique=False)
    op.create_index('ix_notification_action_type_slug', 'notification_action_type', ['slug'], unique=False)
    op.create_index('ix_notification_action_type_created_at', 'notification_action_type', ['created_at'], unique=False)
    op.create_index('ix_notification_action_type_code', 'notification_action_type', ['code'], unique=True)
    # ### end Alembic commands ###
