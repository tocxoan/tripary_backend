from datetime import datetime
from jinja2 import Environment, FileSystemLoader, select_autoescape

from tripary.clients.mail_gunner import mail_gunner
from tripary.common.schemas import (BaseSchema, ListField, StringField, DatetimeField,
                                    fields, IdField, STRING_LENGTH_VALIDATORS)
from tripary.common.constants import TEMPLATE_DIR
from tripary.databases.postgres import *

from config import BaseConfig


class PayloadForm(BaseSchema):
    itinerary_id = IdField(required=True)

    new_shared_user_ids = ListField(IdField(), allow_none=True)
    new_shared_group_ids = ListField(IdField(), allow_none=True)


class Handler(object):
    def __init__(self, session, sender_manager):
        self.session = session
        self.sender_manager = sender_manager

    def parse_payload(self, raw):
        schema = PayloadForm()
        parsed = schema.load(raw)
        if parsed.errors:
            raise Exception(parsed.errors)
        return parsed.data

    def handle(self, itinerary_id,
               new_shared_user_ids=None,
               new_shared_group_ids=None):

        if not new_shared_group_ids:
            new_shared_group_ids = []

        if not new_shared_user_ids:
            new_shared_user_ids = []

        it = self.session.query(Itinerary).get(itinerary_id)
        default_image = self.session.query(Image).filter(
            Image.object_id == itinerary_id,
            Image.object_type == 'itinerary',
            Image.is_default.is_(True),
        ).first()
        if default_image:
            default_image_url = default_image.url
        else:
            default_image_url = 'https://storage.googleapis.com/tripary.me/itinerary_default_image.jpg'

        direct_shared_emails = list(map(
            lambda x: x.email,
            self.session.query(User).filter(User.id.in_(new_shared_user_ids)).all()
        ))

        shared_groups = self.session.query(Group).filter(
            Group.id.in_(new_shared_group_ids)
        ).all()

        template_env = Environment(
            loader=FileSystemLoader(TEMPLATE_DIR),
            autoescape=select_autoescape(['html', 'xml'])
        )
        email_template = template_env.get_template('mail_content/itinerary_share.html')
        redirect_url = BaseConfig.WEB_DOMAIN + '/app/view/itinerary/' + itinerary_id

        if direct_shared_emails:
            subject = '%s đã chia sẻ cho bạn lịch trình - %s' % (it.creator.email, it.name)
            mail_gunner.send(to_addr=direct_shared_emails[0],
                             cc=direct_shared_emails,
                             subject=subject,
                             body=email_template.render(
                                 share_type='direct',
                                 it=it,
                                 redirect_url=redirect_url,
                                 default_image_url=default_image_url
                             ))

        for group in shared_groups:
            subject = '%s đã chia sẻ vào nhóm %s lịch trình - %s' % (
                it.creator.name or it.creator.email,
                group.name,
                it.name
            )
            to_addrs = list(filter(lambda x: x.user_id != it.creator_id, group.members))
            to_addrs = list(map(lambda x: x.user.email, to_addrs))
            mail_gunner.send(to_addr=to_addrs[0],
                             cc=to_addrs,
                             subject=subject,
                             body=email_template.render(
                                 share_type='group',
                                 it=it,
                                 redirect_url=redirect_url,
                                 group=group,
                                 default_image_url=default_image_url
                             ))

    def run(self, payload):
        payload = self.parse_payload(payload)

        try:
            self.handle(**payload)
        except Exception:
            raise
        return True
