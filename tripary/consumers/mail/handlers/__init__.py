from .it_sharing_handler import Handler as ItSharingHandler
from .group_inviting_handler import Handler as GroupInvitingHandler

HANDLERS = {
    'it-sharing': ItSharingHandler,
    'group-inviting': GroupInvitingHandler
}
