from datetime import datetime
from jinja2 import Environment, FileSystemLoader, select_autoescape

from tripary.clients.mail_gunner import mail_gunner
from tripary.common.schemas import (BaseSchema, ListField, StringField, DatetimeField,
                                    fields, IdField, STRING_LENGTH_VALIDATORS)
from tripary.common.constants import TEMPLATE_DIR
from tripary.databases.postgres import *

from config import BaseConfig


class PayloadForm(BaseSchema):
    group_id = IdField(required=True)

    user_ids = ListField(IdField(), required=True)


class Handler(object):
    def __init__(self, session, sender_manager):
        self.session = session
        self.sender_manager = sender_manager

    def parse_payload(self, raw):
        schema = PayloadForm()
        parsed = schema.load(raw)
        if parsed.errors:
            raise Exception(parsed.errors)
        return parsed.data

    def handle(self, group_id, user_ids):

        group = self.session.query(Group).get(group_id)
        creator = self.session.query(User).get(group.creator_id)

        default_image_url = 'https://storage.googleapis.com/tripary.me/group-circle.jpg'

        user_emails = list(map(
            lambda x: x.email,
            self.session.query(User).filter(User.id.in_(user_ids)).all()
        ))

        template_env = Environment(
            loader=FileSystemLoader(TEMPLATE_DIR),
            autoescape=select_autoescape(['html', 'xml'])
        )
        email_template = template_env.get_template('mail_content/group_invitation.html')
        redirect_url = BaseConfig.WEB_DOMAIN + '/group-invitation/' + group_id

        if user_emails:
            subject = '%s đã mời bạn vào nhóm - %s' % (creator.email, group.name)
            mail_gunner.send(to_addr=user_emails[0],
                             cc=user_emails,
                             subject=subject,
                             body=email_template.render(
                                 share_type='direct',
                                 group=group,
                                 creator=creator,
                                 redirect_url=redirect_url,
                                 default_image_url=default_image_url
                             ))

    def run(self, payload):
        payload = self.parse_payload(payload)

        try:
            self.handle(**payload)
        except Exception:
            raise
        return True
