from .mail import Consumer as MailConsumer

CONSUMERS = {
    'mail': MailConsumer
}

__all__ = (
    'CONSUMERS'
)
