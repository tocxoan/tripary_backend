from datetime import datetime

from tripary.common.schemas import (BaseSchema, StringField, DatetimeField,
                                    fields, IdField, STRING_LENGTH_VALIDATORS)
from tripary.databases.postgres import *


class PayloadForm(BaseSchema):
    comment_id = IdField(required=True)


class CreationHandler(object):
    def __init__(self, session, sender_manager):
        self.session = session
        self.sender_manager = sender_manager

    def parse_payload(self, raw):
        schema = PayloadForm()
        parsed = schema.load(raw)
        if parsed.errors:
            raise Exception(parsed.errors)
        return parsed.data

    def create(self, user_id, category, timestamp, data, avatar_url=None):
        notification = Notification()
        notification.category = category
        notification.event_time = datetime.utcfromtimestamp(timestamp)
        notification.status = 'created'
        notification.data = data
        notification.user_id = user_id
        if avatar_url:
            notification.avatar_url = avatar_url
        self.session.add(notification)
        self.session.commit()
        return notification

    def run(self, payload):
        payload = self.parse_payload(payload)

        try:
            notification = self.create(**payload)
            self.send(notification)
        except Exception:
            raise
        return True
