from .creation_handler import CreationHandler

HANDLERS = {
    'create': CreationHandler
}
