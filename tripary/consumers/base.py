import pika
import time
from pika import exceptions

from tripary.common.utils import log


class BaseConsumer(object):
    def __init__(self,
                 postgres_db,
                 sender_manager,
                 host='localhost',
                 port=5672,
                 virtual_host='/',
                 username='guest',
                 password='guest',
                 exchange='hello-change',
                 exchange_type='direct',
                 queue='hello-queue',
                 routing_key='hello-queue',):

        self.host = host
        self.port = port
        self.virtual_host = virtual_host
        self.username = username
        self.password = password
        self.exchange = exchange
        self.exchange_type = exchange_type
        self.queue = queue
        self.routing_key = routing_key
        self.postgres_db = postgres_db
        self.sender_manager = sender_manager

    def _callback(self, ch, method, properties, body):
        log.info('RECEIVED a message - %s' % body)
        try:
            self.callback(body)
        except Exception as e:
            log.error('%s - %s' % (str(e), body))
        ch.basic_ack(delivery_tag=method.delivery_tag)
        log.info('FINISHED message - %s' % body)

    def callback(self, body):
        """Implement logic handler here"""

    def run(self, with_retry=True, retry_delay=2):
        connection = None

        while True:
            try:
                connection_params = dict(
                    host=self.host,
                    port=self.port,
                    virtual_host=self.virtual_host
                )
                if not self.username or not self.password:
                    credentials = pika.ConnectionParameters._DEFAULT

                else:
                    credentials = pika.PlainCredentials(self.username, self.password)

                connection_params['credentials'] = credentials

                connection = pika.BlockingConnection(pika.ConnectionParameters(**connection_params))

                channel = connection.channel()

                channel.exchange_declare(
                    exchange=self.exchange,
                    exchange_type=self.exchange_type,
                    passive=False,
                    durable=True,
                    auto_delete=False
                )

                channel.queue_declare(
                    queue=self.queue,
                    exclusive=False,
                    auto_delete=False,
                    durable=True,
                    passive=False)

                channel.queue_bind(
                    exchange=self.exchange,
                    queue=self.queue)

                channel.basic_consume(on_message_callback=self._callback,
                                      queue=self.queue,
                                      auto_ack=False)

                log_text = 'Starting:\n'
                for k, v in dict(**self.__dict__,
                                 with_retry=with_retry,
                                 retry_delay=retry_delay).items():
                    log_text += '%s: %s\n' % (k, v)

                log.info(log_text)

                try:
                    channel.start_consuming()

                except KeyboardInterrupt:
                    log.info('Stopping ...')
                    channel.stop_consuming()
                    break

            except pika.exceptions.AMQPConnectionError as e:
                log.debug(e, exc_info=True)
                if not with_retry:
                    break
                else:
                    log.info('Retry in %s', retry_delay)
                    time.sleep(retry_delay)

        if connection:
            connection.close()
