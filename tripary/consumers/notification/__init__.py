from json.decoder import JSONDecodeError

from tripary.consumers.base import BaseConsumer
from tripary.common.utils import log
from tripary.common.schemas import BaseSchema, fields, StringField, STRING_LENGTH_VALIDATORS

from .handlers import HANDLERS


class BodySchema(BaseSchema):
    event_type = StringField(required=True,
                             validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    payload = fields.Dict(default={})


class Consumer(BaseConsumer):
    def parse_body(self, body):
        schema = BodySchema()
        try:
            parsed_data = schema.loads(body)

        except JSONDecodeError:
            raise

        if parsed_data.errors:
            raise Exception(parsed_data.errors)

        return parsed_data.data

    def callback(self, body):
        message = self.parse_body(body)
        event_type = message['event_type']
        payload = message['payload']

        try:
            handler_class = HANDLERS[event_type]
        except KeyError:
            raise Exception('event_type %s is not supported.' % event_type)

        session = self.postgres_db.start_session()
        handler = handler_class(session=session,
                                sender_manager=self.sender_manager)

        try:
            handler.run(payload)
        except Exception:
            session.close()
            raise

        log.info('DONE on message - %s' % body)
        session.close()
