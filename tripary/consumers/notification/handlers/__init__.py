from .creating_handler import CreatingHandler

HANDLERS = {
    'create': CreatingHandler
}
