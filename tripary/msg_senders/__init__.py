import pika
import time
import json
from uuid import uuid4
from pika.exceptions import (ConnectionClosed, ChannelClosed)

from tripary.common.utils import log

from config import MessageQueueConfig


class SenderManager(object):
    def __init__(self):
        self.config_map = {
            'mail': MessageQueueConfig.MAIL
        }

    def get_sender(self, name):
        try:
            config = self.config_map[name]
        except KeyError:
            raise Exception('Sender %s is not supported.' % name)

        return MessageSender(**config)


class MessageSender(object):
    def __init__(self,
                 username='guest',
                 password='guest',
                 host='localhost',
                 port=5672,
                 virtual_host='/',
                 exchange='hello-change',
                 exchange_type='direct',
                 queue='hello-queue',
                 routing_key='hello-queue',
                 ssl=False,
                 ssl_options=None):

        self.username = username
        self.password = password
        self.host = host
        self.port = port
        self.virtual_host = virtual_host
        self.exchange = exchange
        self.exchange_type = exchange_type
        self.queue = queue
        self.routing_key = routing_key
        self.ssl = ssl
        self.ssl_options = ssl_options

        self.connection = self._setup_connection()
        self.producer = self._setup_producer()

    def _setup_connection(self):
        if not self.username or not self.password:
            credentials = pika.ConnectionParameters._DEFAULT
        else:
            credentials = pika.PlainCredentials(self.username, self.password)

        # heartbeat and blocked_connection_timeout refer to this
        # https://pika.readthedocs.io/en/latest/examples/heartbeat_and_blocked_timeouts.html # noqa
        connection_params = dict(
            host=self.host,
            port=self.port,
            virtual_host=self.virtual_host,
            heartbeat=600,
            blocked_connection_timeout=300,
            credentials=credentials
        )
        if self.ssl:
            connection_params['ssl'] = True
            connection_params['ssl_options'] = self.ssl_options

        connection = pika.BlockingConnection(pika.ConnectionParameters(**connection_params))
        return connection

    def _setup_producer(self):
        channel = self.connection.channel()
        channel.exchange_declare(
            exchange=self.exchange,
            exchange_type=self.exchange_type,
            passive=False,
            durable=True,
            auto_delete=False)

        channel.queue_declare(
            queue=self.queue,
            exclusive=False,
            auto_delete=False,
            durable=True
        )

        channel.queue_bind(
            exchange=self.exchange,
            queue=self.queue,
            routing_key=self.routing_key
        )

        channel.confirm_delivery()

        return channel

    def send_message(self, event_type,
                     payload, max_retry=0,
                     retry_interval=5):

        retry_count = 0
        while True:
            message = dict(
                event_type=event_type,
                payload=payload
            )

            properties = pika.BasicProperties()
            properties.content_type = 'application/json'
            properties.message_id = str(uuid4())
            properties.delivery_mode = 2

            log.info('Start sending message - %s' % message)

            try:
                self.producer.basic_publish(body=json.dumps(message),
                                            exchange=self.exchange,
                                            properties=properties,
                                            routing_key=self.routing_key)
                log.info('Message successfully sent - %s' % message)
                return True
            except (ConnectionClosed, ChannelClosed) as e:
                log.error('Send message failed - %s - %s' % (message, str(e)))
                if retry_count >= max_retry:
                    return False
                retry_count += 1
                log.error('Retry %s/%s in %s seconds on message - %s' % (retry_count, max_retry,
                                                                         retry_interval, message))
                time.sleep(retry_interval)
            except Exception as e:
                log.error('Send message failed - %s - %s' % (message, str(e)))
                return False


sender_manager = SenderManager()
