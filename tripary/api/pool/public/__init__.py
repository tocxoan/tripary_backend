from config import PublicApiConfig
from tripary.databases import Postgres
from tripary.api.factory import Factory

from .resources import RESOURCES

sql_db = Postgres(PublicApiConfig.POSTGRES_URI)

factory = Factory(
    app_name=PublicApiConfig.APP_NAME,
    config=PublicApiConfig,
    sql_db=sql_db,
    resources=RESOURCES
)

app = factory.create_app()
