from tripary.common.schemas import (BaseDeletingSchema, BaseCreatingSchema, BaseListingSchema, STRING_LENGTH_VALIDATORS,
                                    StringField, ListField, BaseSchema,
                                    IdField)


class ListingSchema(BaseListingSchema):
    search_text = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], allow_none=True)

    itinerary_id = IdField(required=True)


class CreatingSchema(BaseCreatingSchema):
    itinerary_id = IdField(required=True)

    user_ids = ListField(IdField(), allow_none=True)
    group_ids = ListField(IdField(), allow_none=True)


class DeletingSchema(BaseDeletingSchema):
    itinerary_id = IdField(required=True)


class HandlingShareUrlSchema(BaseSchema):
    itinerary_id = IdField(required=True)
