from tripary.common.schemas import (BaseUpdatingSchema, BaseDeletingSchema, BaseCreatingSchema, BaseListingSchema,
                                    STRING_LENGTH_VALIDATORS, ListField, BaseGettingSchema,
                                    StringField, IdField)


class ListingSchema(BaseListingSchema):
    search_text = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], allow_none=True)


class CreatingSchema(BaseCreatingSchema):
    name = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], required=True)


class UpdatingSchema(BaseUpdatingSchema):
    name = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], allow_none=True)


class DeletingSchema(BaseDeletingSchema):
    pass


class AddingMemberSchema(BaseCreatingSchema):
    group_id = IdField(required=True)
    user_ids = ListField(IdField(), required=True)


class ListingMemberSchema(BaseListingSchema):
    group_id = IdField(required=True)


class RemovingMemberSchema(BaseDeletingSchema):
    pass


class GettingSchema(BaseGettingSchema):
    pass


class InvitationConfirmSchema(BaseGettingSchema):
    pass
