from tripary.common.schemas import (BaseCreatingSchema, BaseListingSchema, STRING_LENGTH_VALIDATORS, StringField, ListField,
                                    IdField, fields)


class CreatingSchema(BaseCreatingSchema):
    object_id = IdField(required=True)

    object_type = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'], required=True)
    content_type = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'], required=True)

    content = fields.Dict(required=True)


class ListingSchema(BaseListingSchema):
    object_id = IdField(required=True)

    object_type = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'], required=True)
