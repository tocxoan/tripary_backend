from tripary.common.schemas import (BaseUpdatingSchema, BaseDeletingSchema, BaseCreatingSchema, BaseListingSchema,
                                    STRING_LENGTH_VALIDATORS, ListField,
                                    StringField, IdField)


class ListingSchema(BaseListingSchema):
    group_id = IdField(required=True)

    search_text = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], allow_none=True)


class CreatingSchema(BaseCreatingSchema):
    group_id = IdField(required=True)
    user_ids = ListField(IdField(), required=True)


class DeletingSchema(BaseDeletingSchema):
    group_id = IdField(required=True)

