from tripary.common.schemas import (BaseUpdatingSchema, BaseDeletingSchema, BaseCreatingSchema, BaseListingSchema,
                                    IdField, StringField,
                                    STRING_LENGTH_VALIDATORS)


class ListingSchema(BaseListingSchema):
    itinerary_id = IdField(required=True)
    order = StringField(default='created_at',
                        missing='created_at',
                        validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])


class ListingPlaceSchema(BaseListingSchema):
    id = IdField(required=True)


class CreatingSchema(BaseCreatingSchema):
    itinerary_id = IdField(required=True)


class DeletingSchema(BaseDeletingSchema):
    itinerary_id = IdField(required=True)


class UpdatingSchema(BaseUpdatingSchema):
    name = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], allow_none=True)
