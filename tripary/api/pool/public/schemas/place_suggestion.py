from tripary.common.schemas import (BaseSchema, BaseDeletingSchema, BaseCreatingSchema, BaseListingSchema,
                                    STRING_LENGTH_VALIDATORS, ListField, fields,
                                    StringField, IdField, CoordinatesSchema)


class CreatingSchema(BaseCreatingSchema):
    event_id = IdField(required=True)
    place_id = IdField(required=True)


class ListingSchema(BaseListingSchema):
    event_id = IdField(required=True)


class DeletingSchema(BaseDeletingSchema):
    pass
