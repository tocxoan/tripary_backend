from tripary.common.schemas import (BaseSchema, BaseDeletingSchema, BaseListingSchema, BaseCreatingSchema,
                                    BaseUpdatingSchema, fields,
                                    IdField, StringField,
                                    STRING_LENGTH_VALIDATORS, TimeField, FileField)


class ListingSchema(BaseListingSchema):
    object_id = IdField(required=True)
    object_type = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])


class CreatingSchema(BaseCreatingSchema):
    object_id = IdField(required=True)
    object_type = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])

    file = FileField(required=True)


class UpdatingSchema(BaseUpdatingSchema):
    public = fields.Boolean(allow_none=True)


class DeletingSchema(BaseDeletingSchema):
    pass
