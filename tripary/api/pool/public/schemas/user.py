from tripary.common.schemas import (BaseSchema, BaseListingSchema, STRING_LENGTH_VALIDATORS, StringField, ListField,
                                    IdField, fields)


class FacebookAuthenticateSchema(BaseSchema):
    uid = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    access_token = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LARGE'])
    name = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    email = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    address = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    avatar_url = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    gender = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    phone = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])


class GoogleAuthenticateSchema(BaseSchema):
    uid = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    token = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['LARGE'])
    name = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    email = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    address = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    avatar_url = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    gender = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
    phone = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])


class ListingSchema(BaseListingSchema):
    search_text = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], allow_none=True)

    fetch_group_ids = fields.Boolean(default=False)

    exclude_user_ids = ListField(IdField(), allow_none=True)
    exclude_share_it_ids = ListField(IdField(), allow_none=True)


class CreatingSchema(BaseSchema):
    email = fields.Email(validate=STRING_LENGTH_VALIDATORS['LONG'], required=True)

    fetch_group_ids = fields.Boolean(default=False)
