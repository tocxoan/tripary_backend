from tripary.common.schemas import (BaseUpdatingSchema, BaseSchema, BaseCreatingSchema, BaseListingSchema,
                                    IdField, StringField,
                                    STRING_LENGTH_VALIDATORS)


class ListingSchema(BaseListingSchema):
    pass


class ReadingSchema(BaseUpdatingSchema):
    pass
