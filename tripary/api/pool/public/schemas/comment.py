from tripary.common.schemas import (BaseCreatingSchema, BaseListingSchema, STRING_LENGTH_VALIDATORS, StringField, ListField,
                                    IdField, fields)


class CreatingSchema(BaseCreatingSchema):
    object_id = IdField(required=True)
    parent_id = IdField(allow_none=True)

    object_type = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'], required=True)
    content = StringField(validate=STRING_LENGTH_VALIDATORS['LARGE'], required=True)
    attachment_type = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'], allow_none=True)

    attachment = fields.Dict(allow_none=True)


class ListingSchema(BaseListingSchema):
    object_id = IdField(required=True)
    parent_id = IdField(allow_none=True)

    object_type = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'], required=True)
    attachment_type = StringField(validate=STRING_LENGTH_VALIDATORS['EX_SHORT'], allow_none=True)
