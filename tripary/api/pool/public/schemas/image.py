from tripary.common.schemas import (BaseSchema, BaseDeletingSchema, BaseListingSchema, BaseCreatingSchema,
                                    BaseUpdatingSchema, fields,
                                    IdField, StringField,
                                    STRING_LENGTH_VALIDATORS, TimeField, FileField)


class ListingSchema(BaseListingSchema):
    object_id = IdField(required=True)
    object_type = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])


class GettingDefaultImageSchema(BaseSchema):
    object_id = IdField(required=True)
    object_type = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])


class CreatingSchema(BaseCreatingSchema):
    object_id = IdField(required=True)
    object_type = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])

    is_default = fields.Boolean(allow_none=True)

    file = FileField(required=True)


class UpdatingSchema(BaseUpdatingSchema):
    desc = StringField(validate=STRING_LENGTH_VALIDATORS['LARGE'], allow_none=True)

    public = fields.Boolean(allow_none=True)


class DeletingSchema(BaseDeletingSchema):
    pass
