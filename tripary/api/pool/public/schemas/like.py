from tripary.common.schemas import (BaseSchema, BaseGettingSchema, BaseCreatingSchema, BaseListingSchema,
                                    STRING_LENGTH_VALIDATORS, ListField, fields,
                                    StringField, IdField, CoordinatesSchema)


class CreatingSchema(BaseCreatingSchema):
    object_id = IdField(required=True)

    object_type = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])


class ListingSchema(BaseListingSchema):
    object_id = IdField(required=True)

    object_type = StringField(required=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])
