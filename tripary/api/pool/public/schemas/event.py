from tripary.common.schemas import (BaseSchema, BaseDeletingSchema, BaseListingSchema, BaseCreatingSchema,
                                    BaseUpdatingSchema, IdField, StringField, fields,
                                    STRING_LENGTH_VALIDATORS, TimeField)


class ListingSchema(BaseListingSchema):
    day_id = IdField(required=True)


class CreatingSchema(BaseCreatingSchema):
    day_id = IdField(required=True)
    category_id = IdField(required=True)
    sub_category_id = IdField(required=True)
    start_place_id = IdField(allow_none=True)
    end_place_id = IdField(allow_none=True)

    start_time = TimeField(allow_none=True)
    end_time = TimeField(allow_none=True)

    name = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    note = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['EX_LARGE'])
    currency = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])

    price = fields.Float(allow_none=True)

    meta = fields.Dict(allow_none=True)


class UpdatingSchema(BaseUpdatingSchema):
    name = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    note = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['EX_LARGE'])
    currency = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])

    start_time = TimeField(allow_none=True)
    end_time = TimeField(allow_none=True)

    start_place_id = IdField(allow_none=True)
    end_place_id = IdField(allow_none=True)
    category_id = IdField(required=True)
    sub_category_id = IdField(required=True)

    price = fields.Float(allow_none=True)

    meta = fields.Dict(allow_none=True)


class DeletingSchema(BaseDeletingSchema):
    day_id = IdField(required=True)


class MovingSchema(BaseUpdatingSchema):
    day_id = IdField(required=True)
