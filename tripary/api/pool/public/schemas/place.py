from tripary.common.schemas import (BaseSchema, BaseGettingSchema, BaseCreatingSchema, BaseListingSchema,
                                    STRING_LENGTH_VALIDATORS, ListField, fields,
                                    StringField, IdField, CoordinatesSchema)


class AddressComponentSchema(BaseSchema):
    types = fields.List(StringField(validate=STRING_LENGTH_VALIDATORS['LONG']), required=True)
    long_name = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], required=True)
    short_name = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], required=True)


class CreatingSchema(BaseCreatingSchema):
    map_id = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], required=True)
    name = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], required=True)
    map_url = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], allow_none=True)
    website = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], allow_none=True)
    phone = StringField(validate=STRING_LENGTH_VALIDATORS['LONG'], allow_none=True)
    address = StringField(validate=STRING_LENGTH_VALIDATORS['LARGE'], allow_none=True)

    address_components = fields.List(fields.Nested(AddressComponentSchema), allow_none=True)
    images = fields.List(StringField(validate=STRING_LENGTH_VALIDATORS['LARGE']), allow_none=True)
    place_types = fields.List(StringField(validate=STRING_LENGTH_VALIDATORS['LONG']), required=True)

    coordinates = fields.Nested(CoordinatesSchema, required=True)

    viewport = fields.Dict(allow_none=True)


class GettingSchema(BaseGettingSchema):
    pass
