from tripary.common.schemas import (BaseSchema, BaseGettingSchema, STRING_LENGTH_VALIDATORS, StringField,
                                    BaseUpdatingSchema,
                                    DatetimeField, IdField, ListField, BaseDeletingSchema,
                                    ItineraryStatusField, BaseListingSchema, fields, FileField)


class MetaSchema(BaseSchema):
    cuisine_suggestion = fields.Boolean(default=True)
    stay_suggestion = fields.Boolean(default=True)
    activity_suggestion = fields.Boolean(default=True)
    travel_suggestion = fields.Boolean(default=True)
    suggestion_noti_dialog_disabled = fields.Boolean(default=False)


class UpdatingSchema(BaseUpdatingSchema):
    name = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    desc = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LARGE'])
    start_date = DatetimeField(allow_none=True)

    shared_user_ids = ListField(IdField(), allow_none=True)
    shared_group_ids = ListField(IdField(), allow_none=True)

    status = ItineraryStatusField(allow_none=True)

    meta = fields.Nested(MetaSchema, allow_none=True)


class GettingSchema(BaseGettingSchema):
    full_data = fields.Boolean(default=False)


class DeletingSchema(BaseDeletingSchema):
    pass


class CloningSchema(BaseUpdatingSchema):
    pass


class ListingSchema(BaseListingSchema):
    search_text = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['LONG'])
    progress_state = StringField(allow_none=True, validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])

    group_id = IdField(allow_none=True)

    public_only = fields.Boolean(allow_none=True)
    include_shared = fields.Boolean(allow_none=True)

    exclude_owner_ids = ListField(IdField(), allow_none=True)
    statuses = ListField(ItineraryStatusField(), allow_none=True)
