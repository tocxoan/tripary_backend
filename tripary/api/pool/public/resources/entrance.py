from .base import PublicResource

from ..logics.entrance import EntranceBL


class Entrance(PublicResource):
    GET = dict(
        logic_func='get_entrance_data',
    )

    logic_class = EntranceBL


RESOURCES = {
    '/entrance': Entrance,
}
