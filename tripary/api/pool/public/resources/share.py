from .base import PublicResource

from ..schemas.share import *
from ..logics.share import ShareBL


class ListingShare(PublicResource):
    auth_required = True

    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )

    logic_class = ShareBL


class Share(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create'
    )

    PUT = dict(
        schema=HandlingShareUrlSchema,
        logic_func='handle_share_url'
    )

    DELETE = dict(
        schema=DeletingSchema,
        logic_func='delete'
    )

    logic_class = ShareBL


RESOURCES = {
    '/share': Share,
    '/share/list': ListingShare,
}
