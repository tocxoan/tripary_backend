from .base import PublicResource

from ..schemas.place import *
from ..logics.place import PlaceBL


class Place(PublicResource):

    POST = dict(
        schema=CreatingSchema,
        logic_func='create',
        auth_required=True
    )

    GET = dict(
        schema=GettingSchema,
        logic_func='get'
    )

    logic_class = PlaceBL


RESOURCES = {
    '/place': Place,
}
