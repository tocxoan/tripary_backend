from .base import PublicResource

from ..schemas.like import *
from ..logics.like import LikeBL


class Like(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create',
    )
    logic_class = LikeBL


class ListingLike(PublicResource):
    GET = dict(
        schema=ListingSchema,
        logic_func='list',
    )
    logic_class = LikeBL


RESOURCES = {
    '/like': Like,
    '/like/list': ListingLike,
}
