from .base import PublicResource

from ..schemas.notification import *
from ..logics.notification import NotificationBL


class ListingNotification(PublicResource):
    auth_required = True

    GET = dict(
        schema=ListingSchema,
        logic_func='list',
    )
    logic_class = NotificationBL


class CountingUnseen(PublicResource):
    auth_required = True

    GET = dict(
        logic_func='count_unseen',
    )
    logic_class = NotificationBL


class ReadingNotification(PublicResource):
    auth_required = True

    POST = dict(
        logic_func='read',
        schema=ReadingSchema
    )
    logic_class = NotificationBL


class ReadingAllNotification(PublicResource):
    auth_required = True

    POST = dict(
        logic_func='read_all'
    )
    logic_class = NotificationBL


RESOURCES = {
    '/notification/list': ListingNotification,
    '/notification/unseen/count': CountingUnseen,
    '/notification/read': ReadingNotification,
    '/notification/read-all': ReadingAllNotification,
}
