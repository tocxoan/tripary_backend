from .base import PublicResource

from ..schemas.comment import *
from ..logics.comment import CommentBL


class Comment(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create'
    )

    logic_class = CommentBL


class ListingComment(PublicResource):

    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )
    logic_class = CommentBL


RESOURCES = {
    '/comment': Comment,
    '/comment/list': ListingComment,
}
