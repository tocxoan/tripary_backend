from .base import PublicResource

from ..schemas.place_suggestion import *
from ..logics.place_suggestion import PlaceSuggestionBL


class PlaceSuggestion(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create',
    )

    DELETE = dict(
        schema=DeletingSchema,
        logic_func='delete'
    )

    logic_class = PlaceSuggestionBL


class ListingPlaceSuggestion(PublicResource):
    logic_class = PlaceSuggestionBL

    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )


RESOURCES = {
    '/place/suggestion': PlaceSuggestion,
    '/place/suggestion/list': ListingPlaceSuggestion,
}
