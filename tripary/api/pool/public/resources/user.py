from .base import PublicResource

from ..schemas.user import *
from ..logics.user import UserBL


class FacebookAuthenticate(PublicResource):
    POST = dict(
        schema=FacebookAuthenticateSchema,
        logic_func='fb_auth'
    )

    logic_class = UserBL


class GoogleAuthenticate(PublicResource):
    POST = dict(
        schema=GoogleAuthenticateSchema,
        logic_func='gg_auth'
    )

    logic_class = UserBL


class ListingUser(PublicResource):
    auth_required = True

    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )
    logic_class = UserBL


class User(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create'
    )
    logic_class = UserBL


RESOURCES = {
    '/user/fb-auth': FacebookAuthenticate,
    '/user/gg-auth': GoogleAuthenticate,
    '/user/list': ListingUser,
    '/user': User,
}
