from .base import PublicResource

from ..schemas.event import *
from ..logics.event import EventBL


class ListingEvent(PublicResource):
    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )

    logic_class = EventBL


class MovingEvent(PublicResource):
    POST = dict(
        schema=MovingSchema,
        logic_func='move'
    )

    logic_class = EventBL


class Event(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create'
    )

    PATCH = dict(
        schema=UpdatingSchema,
        logic_func='update'
    )

    DELETE = dict(
        schema=DeletingSchema,
        logic_func='delete'
    )

    logic_class = EventBL


RESOURCES = {
    '/event': Event,
    '/event/list': ListingEvent,
    '/event/move': MovingEvent,
}
