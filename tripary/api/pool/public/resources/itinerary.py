from .base import PublicResource

from ..schemas.itinerary import *
from ..logics.itinerary import ItineraryBL


class Itinerary(PublicResource):
    GET = dict(
        logic_func='get',
        schema=GettingSchema
    )

    POST = dict(
        logic_func='create',
        auth_required=True
    )

    PATCH = dict(
        logic_func='update',
        auth_required=True,
        schema=UpdatingSchema
    )
    DELETE = dict(
        logic_func='delete',
        auth_required=True,
        schema=DeletingSchema
    )

    logic_class = ItineraryBL


class ListingItinerary(PublicResource):
    GET = dict(
        logic_func='list',
        schema=ListingSchema
    )

    logic_class = ItineraryBL


class CloningItinerary(PublicResource):
    auth_required = True
    POST = dict(
        logic_func='clone',
        schema=CloningSchema
    )

    logic_class = ItineraryBL


RESOURCES = {
    '/itinerary': Itinerary,
    '/itinerary/list': ListingItinerary,
    '/itinerary/clone': CloningItinerary,
}
