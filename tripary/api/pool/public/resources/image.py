from .base import PublicResource

from ..schemas.image import *
from ..logics.image import ImageBL


class Image(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create',
        has_file=True,
        param_location='form'
    )

    PATCH = dict(
        schema=UpdatingSchema,
        logic_func='update'
    )

    DELETE = dict(
        schema=DeletingSchema,
        logic_func='delete'
    )
    logic_class = ImageBL


class ListingImage(PublicResource):
    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )

    logic_class = ImageBL


class GettingDefaultImage(PublicResource):
    GET = dict(
        schema=GettingDefaultImageSchema,
        logic_func='get_default',
    )

    logic_class = ImageBL


RESOURCES = {
    '/image': Image,
    '/image/default': GettingDefaultImage,
    '/image/list': ListingImage,
}
