from .base import PublicResource

from ..schemas.attachment import *
from ..logics.attachment import AttachmentBL


class Attachment(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create',
        has_file=True,
        param_location='form'
    )

    PATCH = dict(
        schema=UpdatingSchema,
        logic_func='update'
    )
    DELETE = dict(
        schema=DeletingSchema,
        logic_func='delete'
    )
    logic_class = AttachmentBL


class ListingAttachment(PublicResource):
    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )

    logic_class = AttachmentBL


RESOURCES = {
    '/attachment': Attachment,
    '/attachment/list': ListingAttachment,
}
