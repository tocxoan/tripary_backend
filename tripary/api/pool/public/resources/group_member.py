from .base import PublicResource

from ..schemas.group_member import *
from ..logics.group_member import GroupMemberBL


class GroupMember(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create'
    )

    DELETE = dict(
        schema=DeletingSchema,
        logic_func='delete'
    )

    logic_class = GroupMemberBL


class ListingGroupMember(PublicResource):
    auth_required = True

    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )

    logic_class = GroupMemberBL


RESOURCES = {
    '/group/member': GroupMember,
    '/group/member/list': ListingGroupMember,
}
