from .base import PublicResource

from ..schemas.day import *
from ..logics.day import DayBL


class ListingDay(PublicResource):
    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )

    logic_class = DayBL


class ListingDayPlace(PublicResource):
    GET = dict(
        schema=ListingPlaceSchema,
        logic_func='list_place'
    )

    logic_class = DayBL


class Day(PublicResource):
    auth_required = True
    POST = dict(
        schema=CreatingSchema,
        logic_func='create'
    )
    PATCH = dict(
        schema=UpdatingSchema,
        logic_func='update'
    )
    DELETE = dict(
        schema=DeletingSchema,
        logic_func='delete'
    )
    logic_class = DayBL


RESOURCES = {
    '/day': Day,
    '/day/list': ListingDay,
    '/day/place/list': ListingDayPlace,
}
