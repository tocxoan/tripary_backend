from .base import PublicResource

from ..schemas.message import *
from ..logics.message import MessageBL


class Message(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create'
    )

    logic_class = MessageBL


class ListingMessage(PublicResource):

    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )
    logic_class = MessageBL


RESOURCES = {
    '/message': Message,
    '/message/list': ListingMessage,
}
