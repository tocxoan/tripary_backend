from .base import PublicResource

from ..schemas.group import *
from ..logics.group import GroupBL


class ListingGroup(PublicResource):
    auth_required = True

    GET = dict(
        schema=ListingSchema,
        logic_func='list'
    )

    logic_class = GroupBL


class Group(PublicResource):
    auth_required = True

    POST = dict(
        schema=CreatingSchema,
        logic_func='create'
    )

    PATCH = dict(
        schema=UpdatingSchema,
        logic_func='update'
    )

    DELETE = dict(
        schema=DeletingSchema,
        logic_func='delete'
    )

    logic_class = GroupBL


class GroupInvitationConfirm(PublicResource):
    auth_required = True

    POST = dict(
        schema=InvitationConfirmSchema,
        logic_func='confirm_invitation'
    )
    logic_class = GroupBL


RESOURCES = {
    '/group': Group,
    '/group/list': ListingGroup,
    '/group/invitation/confirm': GroupInvitationConfirm
}
