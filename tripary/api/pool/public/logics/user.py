from slugify import slugify
from sqlalchemy import or_
from google.oauth2 import id_token
from google.auth.transport import requests
from datetime import datetime

from tripary.databases.postgres import User, FacebookInfo, Share, GroupMember, GoogleInfo
from tripary.api.factory.base_logic import BaseLogic
from tripary.api.factory.base_error import BadRequestParams
from tripary.common.utils import make_jwt_token, sort_query, paginate, get_now

from config import ClientsConfig


class UserBL(BaseLogic):

    def fb_auth(self, uid, access_token, **kwargs):
        email = kwargs.get('email')
        user = None

        fb_info = self.session.query(FacebookInfo).filter(
            FacebookInfo.uid == uid
        ).first()

        if fb_info:
            user = fb_info.user

        else:
            if email:
                user = self.session.query(User).filter(
                    User.email == email
                ).first()
            if not user:
                user = User()

            fb_info = FacebookInfo()
            fb_info.uid = uid
            fb_info.user = user

        fb_info.access_token = access_token
        for k, v in kwargs.items():
            if not v:
                continue
            setattr(user, k, v)
            setattr(fb_info, k, v)

        if user.name:
            user.slug = slugify(user.name)

        if user.status == 'in-active':
            user.status = 'active'

        self.session.add(user)
        self.session.add(fb_info)

        self.session.commit()

        jwt_token = make_jwt_token(secret_key=self.app.config['SECRET_KEY'], id=user.id)
        return dict(access_token=jwt_token, auth_user=user.output())

    def gg_auth(self, token, email, **kwargs):

        id_info = id_token.verify_oauth2_token(token,
                                               requests.Request(),
                                               ClientsConfig.GOOGLE_OAUTH['client_id'])
        try:
            if id_info['iss'] not in ['accounts.google.com', 'https://accounts.google.com']:
                raise BadRequestParams('InvalidCredential')

            token_exp_at = datetime.fromtimestamp(id_info['exp'])
            if token_exp_at < get_now():
                raise BadRequestParams('InvalidCredential')

            _email = id_info['email']
            if email != _email:
                raise BadRequestParams('InvalidCredential')

            uid = id_info['sub']

            person_params = dict(
                email=_email,
                name=id_info['name'],
                avatar_url=id_info['picture']
            )

        except KeyError:
            raise BadRequestParams('InvalidCredential')

        gg_info = self.session.query(GoogleInfo).filter(
            GoogleInfo.uid == uid
        ).first()

        if gg_info:
            user = gg_info.user

        else:
            gg_info = GoogleInfo()

            user = self.session.query(User).filter(
                User.email == email
            ).first()

            if not user:
                user = User()

            gg_info.user = user

        gg_info.token = token
        gg_info.uid = uid
        for k, v in person_params.items():
            if not v:
                continue
            setattr(user, k, v)
            setattr(gg_info, k, v)

        if user.name:
            user.slug = slugify(user.name)

        if user.status == 'in-active':
            user.status = 'active'

        self.session.add(user)
        self.session.add(gg_info)

        self.session.commit()

        jwt_token = make_jwt_token(secret_key=self.app.config['SECRET_KEY'], id=user.id)
        return dict(access_token=jwt_token, auth_user=user.output())

    def list(self, page, per_page, order, search_text=None,
             exclude_user_ids=None, exclude_share_it_ids=None,
             fetch_group_ids=None, **kwargs):

        matches = self.session.query(User)

        if search_text:
            matches = matches.filter(
                or_(
                    User.slug.contains(slugify(search_text)),
                    User.email.contains(search_text),
                    User.phone.contains(search_text)
                )
            )
        if not exclude_user_ids:
            exclude_user_ids = []

        if exclude_share_it_ids:
            shares = self.session.query(Share).filter(
                Share.user_id.isnot(None),
                Share.itinerary_id.in_(exclude_share_it_ids)
            )
            exclude_user_ids += list(set(map(lambda x: x.user_id, shares)))

        if exclude_user_ids:
            matches = matches.filter(
                User.id.notin_(exclude_user_ids)
            )

        matches = sort_query(order, User, matches)

        result = []
        for u in paginate(matches, page, per_page):
            result.append(self._output(u, fetch_group_ids=fetch_group_ids))
        return dict(
            total=matches.count(),
            result=result
        )

    def create(self, email, fetch_group_ids=None):
        user = self.session.query(User).filter(
            User.email == email
        ).first()
        if not user:
            user = User(email=email)
        self.session.add(user)
        self.session.commit()
        return self._output(user, fetch_group_ids=fetch_group_ids)

    def _output(self, user, fetch_group_ids=None):
        result = user.output()
        if fetch_group_ids:
            groups = self.session.query(GroupMember).filter(
                GroupMember.user_id == user.id
            )
            result['group_ids'] = list(map(lambda x: x.group_id, groups))
        return result
