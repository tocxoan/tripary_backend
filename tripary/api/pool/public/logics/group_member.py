from slugify import slugify
from sqlalchemy import or_

from tripary.databases.postgres import User, Group, GroupMember
from tripary.api.factory.base_logic import BaseLogic
from tripary.common.utils import sort_query, paginate
from tripary.api.factory.base_error import BadRequestParams, PermissionError


class GroupMemberBL(BaseLogic):

    def _get_group_by_id(self, id):
        group = self.session.query(Group).filter(
            Group.id == id
        ).first()
        if not group:
            raise BadRequestParams('GroupNotFound')
        return group

    def create(self, group_id, user_ids):
        group = self._get_group_by_id(group_id)

        if group.creator_id != self.auth_user.id:
            raise PermissionError

        result = []
        for user_id in user_ids:
            user = self.session.query(User).filter(
                User.id == user_id
            ).first()
            if not user:
                raise BadRequestParams('UserNotFound')

            duplication = self.session.query(GroupMember).filter(
                GroupMember.user_id == user_id,
                GroupMember.group_id == group_id
            ).first()
            if duplication:
                raise BadRequestParams('UserAlreadyInGroup')

            group_user = GroupMember()
            group_user.user_id = user_id
            group_user.group_id = group_id

            self.session.add(group_user)
            result.append(group_user)

        self.session.commit()

        result = [m.output() for m in result]

        mail_sender = self.sm.get_sender('mail')
        mail_sender.send_message(
            event_type='group-inviting',
            payload=dict(
                group_id=group_id,
                user_ids=user_ids
            )
        )

        return result

    def delete(self, ids, group_id):
        group = self._get_group_by_id(group_id)

        is_group_owner = group.creator_id == self.auth_user.id

        for id in ids:
            group_member = self.session.query(GroupMember).filter(
                GroupMember.id == id,
                GroupMember.group_id == group_id
            ).first()
            if not group_member:
                raise BadRequestParams('MemberNotFound')

            if not is_group_owner and group_member.user_id != self.auth_user.id:
                raise PermissionError

            if group_member.user_id == group.creator_id:
                raise BadRequestParams('CannotRemoveGroupOwner')

            self.session.delete(group_member)

        self.session.commit()

        return dict(ids=ids)

    def list(self, page, per_page, order, group_id, search_text=None, **kwargs):
        matches = self.session.query(GroupMember).filter(
            GroupMember.group_id == group_id
        )

        if search_text:
            matches = matches.join(User).filter(
                User.slug.contains(slugify(search_text))
            )

        matches = sort_query(order, GroupMember, matches)
        total = matches.count()
        result = []
        for m in paginate(matches, page, per_page):
            m_output = m.output()
            result.append(m_output)
        return dict(total=total, result=result)
