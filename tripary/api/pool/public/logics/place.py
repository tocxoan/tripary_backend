from slugify import slugify

from tripary.databases.postgres import Place
from tripary.api.factory.base_logic import BaseLogic
from tripary.api.factory.base_error import BadRequestParams


class PlaceBL(BaseLogic):

    def create(self, map_id, coordinates, place_types, name, **kwargs):
        place = self.session.query(Place).filter(
            Place.map_id == map_id
        ).first()
        if not place:
            place = Place()
            place.map_id = map_id

        place.name = name
        place.slug = slugify(name)
        place.coordinates = 'POINT(%s %s)' % (coordinates['lng'], coordinates['lat'])
        place.place_types = ','.join(place_types)

        place = Place.convert_from_dict(kwargs, origin=place)

        self.session.add(place)
        self.session.commit()
        return place.output()

    def get(self, id):
        place = self.session.query(Place).get(id)
        if not place:
            raise BadRequestParams('PlaceNotFound')
        return place.output()
