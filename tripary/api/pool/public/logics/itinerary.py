import asyncio
from sqlalchemy import or_
from slugify import slugify
from datetime import timedelta

from tripary.databases.postgres import (Itinerary, Day, Event, Attachment, Comment,
                                        Place, Image, Share, GroupMember, User, Group)
from tripary.api.factory.base_logic import BaseLogic
from tripary.common.utils import get_now, sort_query, paginate
from tripary.api.factory.base_error import BadRequestParams, PermissionError, ServerError
from tripary.api.factory.utils import get_itinerary_share


class ItineraryBL(BaseLogic):

    @staticmethod
    def _get_default_image(itinerary_id, session):
        default_image = session.query(Image).filter(
            Image.is_default.is_(True),
            Image.object_id == itinerary_id,
            Image.object_type == 'itinerary'
        ).first()
        if default_image:
            return default_image.output()
        return None

    def _output(self, itinerary, only_fields=None,
                exclude_fields=None, **kwargs):

        result = itinerary.output(excludes=exclude_fields,
                                  includes=only_fields)
        result['default_image'] = self._get_default_image(itinerary.id, self.session)
        return result

    def create(self):
        now = get_now()

        itinerary = Itinerary()
        itinerary.name = 'Chuyến đi tuyệt vời của %s' % self.auth_user.name or self.auth_user.email
        itinerary.slug = slugify(itinerary.name)
        itinerary.start_date = now.replace(minute=0, second=0,
                                           hour=0, microsecond=0)
        itinerary.creator_id = self.auth_user.id

        day = Day()
        day.itinerary = itinerary

        self.session.add(day)
        self.session.add(itinerary)
        self.session.commit()

        return self._output(itinerary)

    def update(self, id, **kwargs):
        it = self._get(id, Itinerary)

        if not it:
            raise BadRequestParams('ItineraryNotFound')
        if it.creator_id != self.auth_user.id:
            raise PermissionError

        it.is_draft = False

        name = kwargs.pop('name', None)
        if name:
            it.name = name
            it.slug = slugify(name)

        status = kwargs.pop('status', None)
        shared_user_ids = kwargs.pop('shared_user_ids', [])
        shared_group_ids = kwargs.pop('shared_group_ids', [])
        new_shared_group_ids = []
        new_shared_user_ids = []
        if status:
            if status == 'share':

                current_shared_users = self.session.query(Share).filter(
                    Share.user_id.isnot(None),
                    Share.itinerary_id == it.id
                )

                current_shared_groups = self.session.query(Share).filter(
                    Share.group_id.isnot(None),
                    Share.itinerary_id == it.id
                )

                for u_s in current_shared_users:
                    if u_s.user_id not in shared_user_ids:
                        self.session.delete(u_s)

                for g_s in current_shared_groups:
                    if g_s.group_id not in shared_group_ids:
                        self.session.delete(g_s)

                current_shared_user_ids = list(map(lambda x: x.user_id, current_shared_users))

                for user_id in shared_user_ids:
                    user = self.session.query(User).get(user_id)
                    if not user:
                        raise BadRequestParams('UserNotFound', payload=user_id)
                    if user.id in current_shared_user_ids:
                        continue
                    share = Share()
                    share.itinerary_id = it.id
                    share.user_id = user_id
                    self.session.add(share)

                    new_shared_user_ids.append(user.id)

                current_shared_group_ids = list(map(lambda x: x.group_id, current_shared_groups))
                for group_id in shared_group_ids:
                    group_member = self.session.query(GroupMember).filter(
                        GroupMember.group_id == group_id,
                        GroupMember.user_id == self.auth_user.id
                    ).first()
                    if not group_member:
                        raise BadRequestParams('GroupNotFound')
                    if group_member.group_id in current_shared_group_ids:
                        continue
                    share = Share()
                    share.itinerary_id = it.id
                    share.group_id = group_id
                    self.session.add(share)

                    new_shared_group_ids.append(group_id)

            it.status = status

        it = Itinerary.convert_from_dict(kwargs, origin=it)

        self.session.add(it)
        self.session.commit()

        if it.status == 'share' and (new_shared_user_ids or new_shared_group_ids):
            mail_sender = self.sm.get_sender('mail')
            mail_sender.send_message(
                event_type='it-sharing',
                payload=dict(
                    itinerary_id=it.id,
                    new_shared_user_ids=new_shared_user_ids,
                    new_shared_group_ids=new_shared_group_ids
                )
            )

        return self._output(it)

    def get(self, id, **kwargs):
        it = self._get(id, Itinerary)
        if not it:
            raise BadRequestParams('ItineraryNotFound')

        if it.status != 'public':
            if not self.auth_user:
                raise PermissionError
            share = get_itinerary_share(self.session, it, self.auth_user)
            if not share and self.auth_user.id != it.creator_id:
                raise PermissionError

        return self._output(it)

    def list(self, page, per_page, order,
             public_only=None, progress_state=None,
             group_id=None, exclude_owner_ids=None,
             include_shared=None, **kwargs):
        matches = self.session.query(Itinerary).filter(
            Itinerary.deleted.is_(False)
        )

        # if owned_only or shared_only or group_id:
        #     if not self.auth_user:
        #         raise PermissionError
        #
        #     if owned_only:
        #         matches = matches.filter(
        #             Itinerary.creator_id == self.auth_user.id
        #         )
        #     elif shared_only:
        #         matches = matches.outerjoin(Share).outerjoin(
        #             GroupMember, Share.group_id == GroupMember.group_id
        #         ).filter(
        #             or_(
        #                 Share.user_id == self.auth_user.id,
        #                 GroupMember.user_id == self.auth_user.id
        #             )
        #         )
        #     else:
        #         group_member = self.session.query(GroupMember).filter(
        #             GroupMember.group_id == group_id,
        #             GroupMember.user_id == self.auth_user.id
        #         ).first()
        #
        #         if not group_member:
        #             raise PermissionError
        #
        #         matches = matches.join(Share).filter(
        #             Share.group_id == group_id
        #         )
        #
        #     statuses = kwargs.get('statuses')
        #     if statuses:
        #         matches = matches.filter(Itinerary.status.in_(statuses))

        if exclude_owner_ids:
            matches = matches.filter(
                Itinerary.creator_id.notin_(exclude_owner_ids)
            )

        if not self.auth_user or public_only:
            matches = matches.filter(
                Itinerary.status == 'public'
            )
        else:
            if group_id:
                group_member = self.session.query(GroupMember).filter(
                    GroupMember.group_id == group_id,
                    GroupMember.user_id == self.auth_user.id
                ).first()

                if not group_member:
                    raise PermissionError

                matches = matches.join(Share).filter(
                    Share.group_id == group_id,
                    Itinerary.status == 'share'
                )
            elif include_shared:
                shares = self.session.query(Share).join(Itinerary).outerjoin(
                    GroupMember, GroupMember.group_id == Share.group_id
                ).filter(
                    Itinerary.status == 'share',
                    or_(
                        Share.user_id == self.auth_user.id,
                        GroupMember.user_id == self.auth_user.id
                    )
                )

                matches = matches.filter(
                    or_(
                        Itinerary.id.in_(list(map(lambda x: x.itinerary_id, shares))),
                        Itinerary.creator_id == self.auth_user.id
                    )
                )
            else:
                pass

        if progress_state:
            now = get_now()

            if progress_state == 'planning':
                matches = matches.filter(
                    Itinerary.start_date > now
                )
            elif progress_state == 'ongoing':
                matches = matches.filter(
                    Itinerary.start_date <= now,
                    Itinerary.end_date > now
                )
            else:
                matches = matches.filter(
                    Itinerary.end_date <= now
                )

        search_text = kwargs.get('search_text')
        if search_text:
            matches = matches.filter(
                Itinerary.slug.contains(slugify(search_text))
            )

        matches = sort_query(order, Itinerary, matches)
        total = matches.count()
        result = []
        for i in paginate(matches, page, per_page):
            result.append(self._output(i))

        return dict(total=total, result=result)

    def delete(self, ids):
        for id in ids:
            it = self._get(id, Itinerary)
            if not it:
                continue

            if it.creator_id != self.auth_user.id:
                raise PermissionError

            it.deleted = True

            self.session.add(it)
        self.session.commit()
        return dict(ids=ids)

    def clone(self, id):
        itinerary = self._get(id, Itinerary)
        if not itinerary:
            raise BadRequestParams('ItineraryNotFound')
        now = get_now()

        # clone it
        clone_it = Itinerary()
        clone_it.name = itinerary.name + ' - copy'
        clone_it.slug = slugify(clone_it.name)
        clone_it.creator_id = self.auth_user.id
        clone_it.start_date = itinerary.start_date

        event_map = []

        # clone day
        itinerary.days.sort(key=lambda x: x.created_at)
        for index, day in enumerate(itinerary.days):
            clone_day = Day()
            if day.name:
                clone_day.name = day.name
                clone_day.slug = slugify(day.name)
                clone_day.created_at = now + timedelta(seconds=index)
            clone_day.itinerary = clone_it
            self.session.add(clone_day)

            # clone event
            for event in day.events:
                clone_event = event.clone(excludes=['itinerary_id', 'day_id', 'comment_count'])
                clone_event.itinerary = clone_it
                clone_event.day = clone_day
                self.session.add(clone_event)
                event_map.append([event.id, clone_event])

        self.session.add(clone_it)
        self.session.commit()

        # clone cover image
        default_image = self.session.query(Image).filter(
            Image.object_type == 'itinerary',
            Image.object_id == itinerary.id,
            Image.is_default.is_(True)
        ).first()
        if default_image:
            clone_default_image = default_image.clone(excludes=['object_id', 'creator_id'])
            clone_default_image.object_id = clone_it.id
            clone_default_image.creator_id = self.auth_user.id

            self.session.add(clone_default_image)

        is_shared = get_itinerary_share(self.session, itinerary, self.auth_user)

        for origin_id, c_e in event_map:
            # clone event image
            images = self.session.query(Image).filter(
                Image.object_type == 'event',
                Image.object_id == origin_id
            )
            if not is_shared and self.auth_user.id != itinerary.creator_id:
                images = images.filter(
                    Image.public.is_(True)
                )

            for im in images:
                clone_image = im.clone(excludes=['object_id', 'creator_id'])
                clone_image.object_id = c_e.id
                clone_image.creator_id = self.auth_user.id
                self.session.add(clone_image)

            # clone attachments
            atts = self.session.query(Attachment).filter(
                Attachment.object_id == origin_id,
                Attachment.object_type == 'event'
            )
            if not is_shared and self.auth_user.id != itinerary.creator_id:
                atts = atts.filter(
                    Attachment.public.is_(True)
                )
            for a in atts:
                clone_a = a.clone(excludes=['object_id', 'creator_id'])
                clone_a.object_id = c_e.id
                clone_a.creator_id = self.auth_user.id

                self.session.add(clone_a)
        self.session.commit()
        return self._output(clone_it)
