from slugify import slugify
from sqlalchemy import or_

from tripary.databases.postgres import Message
from tripary.api.factory.base_logic import BaseLogic
from tripary.common.utils import sort_query, paginate
from tripary.api.factory.base_error import BadRequestParams, PermissionError
from tripary.api.factory.utils import get_itinerary_share

OBJECT_TYPE_MAP = {
}


class MessageBL(BaseLogic):

    def _get_object(self, object_type, object_id):
        try:
            table = OBJECT_TYPE_MAP[object_type]
        except KeyError:
            raise BadRequestParams('UnsupportedObjectType')

        obj = self.session.query(table).filter(
            table.id == object_id
        ).first()
        if not obj:
            raise BadRequestParams('ObjectNotFound')

        return obj

    def _check_permission(self, obj, object_type):
        if object_type in ['place-event']:
            it = obj.event.itinerary
            share = get_itinerary_share(self.session, it, self.auth_user)
            if not share and self.auth_user.id != it.creator_id:
                raise PermissionError

    def _handle_content(self, content_type, content):
        return content

    def list(self, page, per_page, order, object_id, object_type, **kwargs):

        obj = self._get_object(object_type, object_id)

        self._check_permission(obj, object_type)

        matches = self.session.query(Message).filter(
            Message.object_type == object_type,
            Message.object_id == object_id,
        )

        matches = sort_query(order, Message, matches)
        total = matches.count()
        result = []
        for m in paginate(matches, page, per_page):
            result.append(m.output())

        return dict(total=total, result=result)

    def create(self, object_id, object_type, content_type, content, **kwargs):
        obj = self._get_object(object_type, object_id)

        self._check_permission(obj, object_type)

        message = Message()
        message.creator_id = self.auth_user.id
        message.object_type = object_type
        message.object_id = object_id
        message.content = self._handle_content(content_type, content)
        message.content_type = content_type

        self.session.add(message)
        self.session.commit()
        return message.output()
