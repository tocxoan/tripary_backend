import os
from slugify import slugify

from tripary.databases.postgres import (Itinerary, Event, Image)
from tripary.api.factory.base_logic import BaseLogic
from tripary.common.utils import sort_query, paginate
from tripary.common.constants import APP_ROOT_DIR, MAX_IMAGE_COUNT
from tripary.api.factory.base_error import BadRequestParams, ServerError, PermissionError
from tripary.clients import google_storage_client
from tripary.clients.google_storage import errors as gs_errors
from tripary.api.factory.utils import get_itinerary_share

OBJECT_TYPE_MAP = {
    'itinerary': Itinerary,
    'event': Event,
    'event-comment': Event
}


class ImageBL(BaseLogic):
    def _get_object(self, object_type, object_id):
        try:
            table = OBJECT_TYPE_MAP[object_type]
        except KeyError:
            raise BadRequestParams('UnsupportedObjectType')
        obj = self.session.query(table).filter(
            table.id == object_id
        ).first()

        if not obj:
            raise BadRequestParams('ObjectNotFound')
        return obj

    def _handle_path(self, obj, object_type):
        if object_type == 'event':
            return [obj.itinerary_id, obj.day_id, obj.id]
        if object_type == 'itinerary':
            return [obj.id]

    def _check_permission(self, object_type, object_id):
        pass

    def list(self, page, per_page, order, object_id, object_type, **kwargs):
        obj = self._get_object(object_type, object_id)

        matches = self.session.query(Image).filter(
            Image.object_id == obj.id,
            Image.object_type == object_type,
        )

        if self.auth_user:
            if object_type == 'event':
                is_shared = get_itinerary_share(self.session, obj.itinerary, self.auth_user)
                if not is_shared and self.auth_user.id != obj.itinerary.creator_id:
                    matches = matches.filter(Image.public.is_(True))
        else:
            matches = matches.filter(
                Image.public.is_(True)
            )

        matches = sort_query(order, Image, matches)

        total = matches.count()
        result = [i.output() for i in paginate(matches, page, per_page)]
        return dict(total=total, result=result)

    def get_default(self, object_type, object_id):
        obj = self._get_object(object_type, object_id)

        default_image = self.session.query(Image).filter(
            Image.object_id == obj.id,
            Image.object_type == object_type,
            Image.is_default.is_(True)
        ).first()
        if not default_image:
            return None
        return default_image.output()

    def create(self, object_id, object_type, file, is_default=None):
        obj = self._get_object(object_id=object_id,
                               object_type=object_type)

        self._check_permission(object_type, object_id)

        current_image_count = self.session.query(Image).filter(
            Image.object_id == object_id,
            Image.object_type == object_type
        ).count()

        if current_image_count >= MAX_IMAGE_COUNT:
            raise BadRequestParams('ReachMaximumImage')

        image = Image()
        image.object_type = object_type
        image.object_id = object_id
        image.creator_id = self.auth_user.id
        image.name = file.name
        image.slug = slugify(file.name)
        image.content_type = file.type

        path = self._handle_path(obj, object_type)

        tmp_dir = os.path.join(APP_ROOT_DIR, 'tmp')
        if not os.path.exists(tmp_dir):
            os.makedirs(tmp_dir)

        tmp_path = os.path.join(tmp_dir, slugify(image.name))
        with open(tmp_path, 'wb') as wb_file:
            wb_file.write(file.body)

        try:
            upload_data = google_storage_client.upload(file_type='image',
                                                       file_name=image.name,
                                                       file_path=tmp_path,
                                                       path=path)
        except (gs_errors.ContentTypeNotSupported, gs_errors.FileTypeNotSupported) as e:
            raise BadRequestParams(e.__class__.__name__)
        except Exception as e:
            raise ServerError(e)

        os.remove(tmp_path)

        image.size = upload_data['size']
        image.url = upload_data['url']
        image.path = upload_data['path']

        if is_default:
            current_default = self.session.query(Image).filter(
                Image.is_default.is_(True),
                Image.object_id == object_id,
                Image.object_type == object_type
            ).first()
            if current_default:
                current_default.is_default = False
                self.session.add(current_default)
            image.is_default = True
        self.session.add(image)
        self.session.commit()
        return image.output()

    def update(self, id, desc=None, public=None, **kwargs):
        image = self.session.query(Image).get(id)
        if not image:
            raise BadRequestParams('ImageNotFound')
        if image.creator_id != self.auth_user.id:
            raise PermissionError
        if desc is not None:
            image.desc = desc
        if public is not None:
            image.public = public
        self.session.add(image)
        self.session.commit()
        return image.output()

    def delete(self, ids):
        for id in ids:
            image = self.session.query(Image).get(id)
            if not image:
                continue

            self._check_permission(image.object_type, image.object_id)

            self.session.delete(image)

        self.session.commit()
        return dict(ids=ids)
