from tripary.databases.postgres import Notification
from tripary.api.factory.base_logic import BaseLogic
from tripary.common.utils import sort_query, paginate, get_now
from tripary.api.factory.base_error import BadRequestParams, PermissionError


class NotificationBL(BaseLogic):
    def list(self, page, per_page, order, **kwargs):
        matched_records = self.session.query(Notification).filter(
            Notification.user_id == self.auth_user.id
        )

        matched_records.filter(
            Notification.status == 'created'
        ).update({
            Notification.status: 'seen',
            Notification.seen_at: get_now()
        }, synchronize_session=False)

        self.session.commit()

        matched_records = sort_query(order, Notification, matched_records)

        total = matched_records.count()

        result = []
        for n in paginate(matched_records, page, per_page):
            output = n.output()
            result.append(output)

        return dict(total=total, result=result)

    def count_unseen(self):
        result = self.session.query(Notification).filter(
            Notification.user_id == self.auth_user.id,
            Notification.status == 'created'
        ).count()
        return dict(result=result)

    def read(self, id):
        notification = self.session.query(Notification).get(id)
        if not notification:
            raise BadRequestParams('NotificationNotFound')
        if notification.status != 'read':
            notification.status = 'read'
            notification.read_at = get_now()
            self.session.add(notification)
        self.session.commit()
        return notification.output()

    def read_all(self):
        self.session.query(Notification).filter(
            Notification.status != 'read',
            Notification.user_id == self.auth_user.id
        ).update({
            Notification.status: 'read',
            Notification.read_at: get_now()
        }, synchronize_session=False)

        self.session.commit()
        return dict(success=True)
