import asyncio

from tripary.databases.postgres import (Event, Comment, Place,
                                        NotificationActionType, NotificationEventCreator,
                                        Notification, Image)
from tripary.api.factory.base_logic import BaseLogic
from tripary.common.utils import paginate, get_now
from tripary.common.constants import NOTIFICATION_TYPE_INTERVAL
from tripary.api.factory.base_error import BadRequestParams, PermissionError
from tripary.api.factory.utils import get_itinerary_share, sort_query

OBJECT_TYPE_MAP = {
    'event': Event
}


class CommentBL(BaseLogic):

    def _get_object(self, object_type, object_id):
        try:
            table = OBJECT_TYPE_MAP[object_type]
        except KeyError:
            raise BadRequestParams('UnsupportedObjectType')

        obj = self.session.query(table).filter(
            table.id == object_id
        ).first()
        if not obj:
            raise BadRequestParams('ObjectNotFound')

        return obj

    def _check_permission(self, obj, object_type):
        if object_type in ['event', 'day']:
            it = obj.itinerary
            if it.status == 'private':
                share = get_itinerary_share(self.session, it, self.auth_user)
                if not share and self.auth_user.id != it.creator_id:
                    raise PermissionError

    def _handle_attachment(self, attachment_type, data):
        result = {}
        if attachment_type == 'place':
            try:
                place_data = data['place']
                place_id = place_data['id']
            except KeyError:
                raise BadRequestParams('InvalidAttachmentContent')

            place = self.session.query(Place).get(place_id)
            if not place:
                raise BadRequestParams('PlaceNotFound')
            result['place'] = place.output(includes=[
                'name', 'address', 'website',
                'phone', 'coordinates', 'viewport'
            ])

        elif attachment_type == 'image':
            try:
                image_id = data['id']
                object_type = data['object_type']
                object_id = data['object_id']
            except KeyError:
                raise BadRequestParams('InvalidAttachmentContent')

            image = self.session.query(Image).filter(
                Image.id == image_id,
                Image.object_type == object_type,
                Image.object_id == object_id,
            ).first()
            if not image:
                raise BadRequestParams('ImageNotFound')

            result = data

        return result

    def list(self, page, per_page, sort_by,
             object_id, object_type, parent_id=None, **kwargs):

        obj = self._get_object(object_type, object_id)

        self._check_permission(obj, object_type)

        matches = self.session.query(Comment).filter(
            Comment.object_type == object_type,
            Comment.object_id == object_id,
        )

        if parent_id:
            matches = matches.filter(
                Comment.parent_id == parent_id
            )

        else:
            matches = matches.filter(
                Comment.parent_id.is_(None)
            )

        attachment_type = kwargs.get('attachment_type')
        if attachment_type:
            matches = matches.filter(
                Comment.attachment_type == attachment_type
            )
        matches = sort_query(matches, Comment, sort_by)
        total = matches.count()
        result = []
        for m in paginate(matches, page, per_page):
            output = m.output()
            result.append(output)

        return dict(total=total, result=result)

    async def create(self, object_id, object_type,
                     content, parent_id=None, attachment_type=None,
                     attachment=None, **kwargs):
        obj = self._get_object(object_type, object_id)

        self._check_permission(obj, object_type)

        comment = Comment()

        if parent_id:
            parent = self.session.query(Comment).filter(
                Comment.id == parent_id
            ).first()
            if not parent:
                raise BadRequestParams('ParentNotFound')
            comment.parent_id = parent_id
            parent.children_count += 1
            self.session.add(parent)

        comment.creator_id = self.auth_user.id
        comment.object_type = object_type
        comment.object_id = object_id
        comment.content = content
        if attachment_type:
            comment.attachment = self._handle_attachment(attachment_type, attachment)
            comment.attachment_type = attachment_type

        if not parent_id:
            obj.comment_count += 1

        self.session.add(comment)
        self.session.add(obj)
        self.session.commit()

        asyncio.ensure_future(self.handle_comment_created(comment.id))

        return comment.output()

    async def handle_comment_created(self, comment_id):
        comment = self.session.query(Comment).get(comment_id)
        if not comment:
            return

        notification = Notification()

        # create event creator object
        notif_event_creator = NotificationEventCreator()
        notif_event_creator.creator_type = 'user'
        notif_event_creator.creator_id = comment.creator_id
        notif_event_creator.event_time = comment.created_at
        notif_event_creator.avatar_url = comment.creator.avatar_url
        notif_event_creator.data = dict(
            comment=comment.output(includes=['object_type', 'object_id',
                                             'parent_id', 'content', 'creator'])
        )

        if comment.object_type == 'event':
            event = self.session.query(Event).get(comment.object_id)
            if not event:
                self.session.close()
                return

            it = event.itinerary

            # check if this comment is a reply
            if comment.parent_id:
                parent = self.session.query(Comment).get(comment.parent_id)
                if not parent:
                    self.session.close()
                    return

                if parent.creator_id == comment.creator_id:
                    self.session.close()
                    return

                notification.action_type_code = 'reply-comment'
                notification.user_id = parent.creator_id
            else:
                if it.creator_id == comment.creator_id:
                    self.session.close()
                    return

                notification.action_type_code = 'comment'
                notification.user_id = it.creator_id

            notif_event_creator.data['itinerary'] = event.itinerary.output(includes=['name',
                                                                                     'start_date',
                                                                                     'creator'])
            notif_event_creator.data['itinerary_id'] = event.itinerary.id
            notif_event_creator.data['day_id'] = event.id
            notif_event_creator.data['event_id'] = event.day_id
            notification.object_type = 'event'
            notification.object_id = event.id
            notification.object_data = event.output()

        # if user_id does not exist,
        # it means this comment does not need to be notified
        if not notification.user_id:
            self.session.close()
            return

        # get the last unread notification of this user
        # with the same type and same object
        last_notification = self.session.query(Notification).filter(
            Notification.user_id == notification.user_id,
            Notification.action_type_code == notification.action_type_code,
            Notification.object_type == notification.object_type,
            Notification.object_id == notification.object_id,
            Notification.status != 'read'
        ).order_by(Notification.created_at.desc()).first()

        # if last notification exists,
        # check if it can be grouped or not
        if last_notification:

            now = get_now()
            # need to be grouped
            if (now - last_notification.created_at).seconds <= NOTIFICATION_TYPE_INTERVAL:

                # check if same event creator
                same_event_creator = self.session.query(NotificationEventCreator).filter(
                    NotificationEventCreator.creator_type == notif_event_creator.creator_type,
                    NotificationEventCreator.creator_id == notif_event_creator.creator_id,
                    NotificationEventCreator.notification_id == last_notification.id
                ).first()

                if same_event_creator:
                    same_event_creator.event_time = notif_event_creator.event_time
                    self.session.add(same_event_creator)
                else:
                    notif_event_creator.notification_id = last_notification.id
                    last_notification.avatar_url = notif_event_creator.avatar_url
                    self.session.add(notif_event_creator)

                last_notification.status = 'created'
                last_notification.seen_at = None
                last_notification.created_at = now
                last_notification.event_time = notif_event_creator.event_time

                self.session.add(last_notification)

            else:
                notification.event_time = notif_event_creator.event_time
                notification.avatar_url = notif_event_creator.avatar_url
                notif_event_creator.notification = notification
                self.session.add(notif_event_creator)
                self.session.add(notification)

        else:
            notification.event_time = notif_event_creator.event_time
            notification.avatar_url = notif_event_creator.avatar_url
            notif_event_creator.notification = notification
            self.session.add(notif_event_creator)
            self.session.add(notification)

        # commit and close session
        self.session.commit()
        self.session.close()
