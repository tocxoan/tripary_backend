from tripary.common.constants import (ITINERARY_STATUSES, USER_STATUSES,
                                      MAX_IMAGE_COUNT, MAX_ATTACHMENT_COUNT)
from tripary.api.factory.base_logic import BaseLogic
from tripary.databases.postgres import EventSubCategory, EventCategory


class EntranceBL(BaseLogic):
    def get_entrance_data(self):
        result = dict(
            constants=dict(
                itinerary_statuses=ITINERARY_STATUSES,
                user_statuses=USER_STATUSES,
                event_categories=[i.output() for i in self.session.query(EventCategory)],
                event_sub_categories=[i.output() for i in self.session.query(EventSubCategory)],
                max_image_count=MAX_IMAGE_COUNT,
                max_attachment_count=MAX_ATTACHMENT_COUNT
            )
        )

        if self.auth_user:
            result['auth_user'] = self.auth_user.output()

        return result
