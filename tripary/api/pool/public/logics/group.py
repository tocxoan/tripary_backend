from slugify import slugify
from sqlalchemy import or_

from tripary.databases.postgres import User, Group, GroupMember
from tripary.api.factory.base_logic import BaseLogic
from tripary.common.utils import sort_query, paginate
from tripary.api.factory.base_error import BadRequestParams, PermissionError


class GroupBL(BaseLogic):

    def _get_group_by_id(self, id):
        group = self.session.query(Group).filter(
            Group.id == id
        ).first()
        if not group:
            raise BadRequestParams('GroupNotFound')
        return group

    def create(self, name):
        duplication = self.session.query(Group).filter(
            Group.slug == slugify(name),
            Group.creator_id == self.auth_user.id
        ).first()
        if duplication:
            raise BadRequestParams('NameAlreadyExists')
        group = Group()
        group.name = name
        group.slug = slugify(name)
        group.creator_id = self.auth_user.id

        group_member = GroupMember()
        group_member.group = group
        group_member.user = self.auth_user

        self.session.add(group)
        self.session.add(group_member)

        self.session.commit()

        return group.output()

    def update(self, id, name=None):
        group = self._get_group_by_id(id)

        if group.creator_id != self.auth_user.id:
            raise PermissionError

        if name:
            duplication = self.session.query(Group).filter(
                Group.slug == slugify(name),
                Group.id != id
            ).first()
            if duplication:
                raise BadRequestParams('NameAlreadyExists')
            group.name = name
            group.slug = slugify(name)

        self.session.add(group)
        self.session.commit()
        return group.output()

    def delete(self, ids):
        for id in ids:
            group = self._get_group_by_id(id)

            if group.creator_id != self.auth_user.id:
                raise PermissionError

            self.session.delete(group)
        self.session.commit()
        return dict(ids=ids)

    def list(self, page, per_page, order, search_text=None, **kwargs):
        matches = self.session.query(Group).outerjoin(GroupMember).filter(
            or_(
                Group.creator_id == self.auth_user.id,
                GroupMember.user_id == self.auth_user.id
            )
        )

        if search_text:
            matches = matches.filter(
                Group.slug.contains(slugify(search_text))
            )

        matches = sort_query(order, Group, matches)
        total = matches.count()
        result = []
        for g in paginate(matches, page, per_page):
            g_output = g.output()
            result.append(g_output)
        return dict(total=total, result=result)

    def confirm_invitation(self, id):
        group = self.session.query(Group).get(id)
        if not group:
            raise BadRequestParams('GroupNotFound')

        member = self.session.query(GroupMember).filter(
            GroupMember.group_id == id,
            GroupMember.user_id == self.auth_user.id
        ).first()

        if not member:
            member = GroupMember()
            member.group_id = id
            member.user_id = self.auth_user.id
            self.session.add(member)
        self.session.commit()

        return member.output()
