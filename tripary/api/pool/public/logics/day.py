from slugify import slugify

from tripary.databases.postgres import (Itinerary, Day, Place, Event, Image, Attachment, Comment)
from tripary.api.factory.base_logic import BaseLogic
from tripary.common.utils import get_now, sort_query, paginate
from tripary.api.factory.base_error import ServerError, BadRequestParams, PermissionError
from tripary.clients import google_storage_client
from tripary.api.factory.utils import get_itinerary_share


class DayBL(BaseLogic):
    def list(self, page, per_page, order, itinerary_id, **kwargs):
        it = self.session.query(Itinerary).filter(
            Itinerary.id == itinerary_id
        ).first()
        if not it:
            raise BadRequestParams('ItineraryNotFound')

        if it.status == 'private':
            if not self.auth_user:
                raise PermissionError
            share = get_itinerary_share(self.session, it, self.auth_user)
            if self.auth_user.id != it.creator_id and not share:
                raise PermissionError

        matches = self.session.query(Day)

        matches = matches.filter(
            Day.itinerary_id == itinerary_id
        )

        matches = sort_query(order, Day, matches)
        total = matches.count()
        result = []
        for d in paginate(matches, page, per_page):
            output = d.output()
            result.append(output)

        return dict(total=total, result=result)

    def create(self, itinerary_id):
        it = self.session.query(Itinerary).filter(
            Itinerary.id == itinerary_id
        ).first()
        if not it:
            raise BadRequestParams('ItineraryNotFound')

        if self.auth_user.id != it.creator_id:
            raise PermissionError

        day = Day()
        day.itinerary_id = itinerary_id
        self.session.add(it)
        self.session.add(day)
        self.session.commit()

        result = day.output()

        return result

    def update(self, id, **kwargs):
        day = self.session.query(Day).filter(
            Day.id == id
        ).first()
        if not day:
            raise BadRequestParams('DayNotFound')

        if self.auth_user.id != day.itinerary.creator_id:
            raise PermissionError

        name = kwargs.pop('name', None)
        if name is not None:
            day.name = name
            day.slug = slugify(name)

        day = Day.convert_from_dict(kwargs, origin=day)
        self.session.add(day)
        self.session.commit()
        return day.output()

    def delete(self, ids, itinerary_id):
        it = self.session.query(Itinerary).filter(
            Itinerary.id == itinerary_id
        ).first()
        if not it:
            raise BadRequestParams('ItineraryNotFound')

        if self.auth_user.id != it.creator_id:
            raise PermissionError

        for id in ids:
            day = self.session.query(Day).filter(
                Day.id == id,
                Day.itinerary_id == itinerary_id
            ).first()
            if not day:
                continue

            event_ids = list(map(lambda x: x.id, day.events))

            # delete all images
            self.session.query(Image).filter(
                Image.object_type.in_(['event', 'comment-event']),
                Image.object_id.in_(event_ids)
            ).delete(synchronize_session=False)

            # delete all attachments
            self.session.query(Attachment).filter(
                Attachment.object_type.in_(['event']),
                Attachment.object_id.in_(event_ids)
            ).delete(synchronize_session=False)

            # delete all comments
            self.session.query(Comment).filter(
                Comment.object_type.in_(['event']),
                Comment.object_id.in_(event_ids)
            ).delete(synchronize_session=False)

            self.session.delete(day)

        self.session.commit()
        return dict(ids=ids)

    def list_place(self, id, page, per_page, order, **kwargs):
        day = self.session.query(Day).get(id)
        if not day:
            raise BadRequestParams('DayNotFound')

        if day.itinerary.status == 'private':
            if not self.auth_user:
                raise PermissionError
            share = get_itinerary_share(self.session, day.itinerary, self.auth_user)
            if self.auth_user.id != day.itinerary.creator_id and not share:
                raise PermissionError
        places = self.session.query(Place).join(Event, Event.start_place_id == Place.id).filter(
            Event.day_id == id
        )
        return dict(
            total=places.count(),
            result=[p.output() for p in places]
        )
