from tripary.databases.postgres import PlaceSuggestion, Like
from tripary.api.factory.base_logic import BaseLogic
from tripary.api.factory.base_error import BadRequestParams
from tripary.common.utils import sort_query, paginate

OBJECT_TYPE_MAP = {
    'place-suggestion': PlaceSuggestion
}


class LikeBL(BaseLogic):

    def _get_obj(self, object_type, object_id):
        try:
            table = OBJECT_TYPE_MAP[object_type]
        except KeyError:
            raise BadRequestParams('UnsupportedObjectType')
        obj = self.session.query(table).get(object_id)
        if not obj:
            raise BadRequestParams('ObjectNotFound')

        return obj

    def create(self, object_type, object_id):

        obj = self._get_obj(object_type, object_id)

        like = self.session.query(Like).filter(
            Like.object_type == object_type,
            Like.object_id == object_id,
            Like.creator_id == self.auth_user.id
        ).first()

        unlike = False

        if like:
            obj.like_count -= 1
            self.session.delete(like)
            unlike = True
        else:
            obj.like_count += 1
            like = Like()
            like.object_id = object_id
            like.object_type = object_type
            like.creator_id = self.auth_user.id
            self.session.add(like)

        self.session.add(obj)
        self.session.commit()

        if unlike:
            result = None
        else:
            result = like.output()

        return result

    def list(self, page, per_page, order, object_type, object_id, **kwargs):
        matches = self.session.query(Like).filter(
            Like.object_id == object_id,
            Like.object_type == object_type,
        )

        matches = sort_query(order, Like, matches)

        total = matches.count()
        result = []

        for l in paginate(matches, page, per_page):
            result.append(l.output())

        return dict(total=total, result=result)
