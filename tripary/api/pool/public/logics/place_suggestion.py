from tripary.databases.postgres import Place, Event, PlaceSuggestion, Like
from tripary.api.factory.base_logic import BaseLogic
from tripary.api.factory.base_error import BadRequestParams, PermissionError
from tripary.api.factory.utils import get_itinerary_share
from tripary.common.utils import sort_query, paginate


class PlaceSuggestionBL(BaseLogic):

    def create(self, event_id, place_id, **kwargs):
        event = self.session.query(Event).get(event_id)
        if not event:
            raise BadRequestParams('EventNotFound')

        if event.itinerary.status == 'private':
            share = get_itinerary_share(self.session, event.itinerary, self.auth_user)
            if self.auth_user.id != event.itinerary.creator_id and not share:
                raise PermissionError

        suggestion = self.session.query(PlaceSuggestion).filter(
            PlaceSuggestion.place_id == place_id,
            PlaceSuggestion.event_id == event_id
        ).first()
        if suggestion:
            raise BadRequestParams('DuplicateSuggestion')

        place = self.session.query(Place).get(place_id)
        if not place:
            raise BadRequestParams('PlaceNotFound')

        suggestion = PlaceSuggestion()
        suggestion.event_id = event_id
        suggestion.place_id = place_id
        self.session.add(suggestion)
        self.session.commit()
        return suggestion.output()

    def delete(self, ids):
        for id in ids:
            suggestion = self.session.query(PlaceSuggestion).get(id)
            if not suggestion:
                continue

            if suggestion.event.itinerary.creator_id != self.auth_user.id:
                raise PermissionError

            self.session.delete(suggestion)
        self.session.commit()
        return dict(ids=ids)

    def list(self, event_id, page, per_page, order, **kwargs):
        matches = self.session.query(PlaceSuggestion).filter(
            PlaceSuggestion.event_id == event_id
        )
        total = matches.count()
        matches = sort_query(order, PlaceSuggestion, matches)
        result = []
        for s in paginate(matches, page, per_page):
            output = s.output()

            if self.auth_user:
                liked = self.session.query(Like).filter(
                    Like.object_id == s.id,
                    Like.object_type == 'place-suggestion',
                    Like.creator_id == self.auth_user.id
                ).first()
                if liked:
                    output['liked'] = liked.output()
            result.append(output)

        return dict(
            total=total,
            result=result
        )
