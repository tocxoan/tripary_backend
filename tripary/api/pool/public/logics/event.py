from slugify import slugify

from tripary.databases.postgres import (Day, Event, Comment, Place, Image, Attachment,
                                        EventCategory, EventSubCategory)
from tripary.api.factory.base_logic import BaseLogic
from tripary.common.utils import sort_query, paginate
from tripary.api.factory.base_error import BadRequestParams, PermissionError
from tripary.api.factory.utils import get_itinerary_share


class EventBL(BaseLogic):
    def count_stuff(self, event, share, result):
        image_count_args = [
            Image.object_id == event.id,
            Image.object_type == 'event'
        ]

        attachment_count_args = [
            Attachment.object_id == event.id,
            Attachment.object_type == 'event'
        ]

        if self.auth_user:
            if self.auth_user.id != event.itinerary.creator_id and not share:
                image_count_args.append(Image.public.is_(True))
                attachment_count_args.append(Attachment.public.is_(True))
        else:
            image_count_args.append(Image.public.is_(True))
            attachment_count_args.append(Attachment.public.is_(True))

        result['image_count'] = self.session.query(Image).filter(
            *image_count_args
        ).count()
        result['attachment_count'] = self.session.query(Attachment).filter(
            *attachment_count_args
        ).count()

    def output(self, event, share=None, **kwargs):
        result = event.output()

        self.count_stuff(event, share, result)

        if event.end_place_id:
            result['end_place'] = self.session.query(Place).get(event.end_place_id).output()

        return result

    def create(self, day_id, category_id, sub_category_id, name=None,
               start_place_id=None, end_place_id=None, **kwargs):
        day = self.session.query(Day).filter(
            Day.id == day_id
        ).first()

        if not day:
            raise BadRequestParams('DayNotFound')

        if day.itinerary.creator_id != self.auth_user.id:
            raise PermissionError

        category = self.session.query(EventCategory).filter(
            EventCategory.id == category_id
        ).first()
        if not category:
            raise BadRequestParams('InvalidCategoryId')

        sub_category = self.session.query(EventSubCategory).filter(
            EventSubCategory.id == sub_category_id,
            EventSubCategory.category_id == category_id
        ).first()
        if not sub_category:
            raise BadRequestParams('InvalidSubCategoryId')

        event = Event()
        event.day_id = day_id
        event.itinerary_id = day.itinerary_id
        event.category_id = category_id
        event.sub_category_id = sub_category_id

        if name:
            event.name = name
            event.slug = slugify(name)

        event = Event.convert_from_dict(kwargs, origin=event)

        if start_place_id:
            s_place = self.session.query(Place).get(start_place_id)
            if not s_place:
                raise BadRequestParams('PlaceNotFound')

            event.start_place_id = start_place_id

        if category.code == 'travel':
            if end_place_id:
                e_place = self.session.query(Place).get(end_place_id)
                if not e_place:
                    raise BadRequestParams('PlaceNotFound')
                event.end_place_id = end_place_id

        self.session.add(event)
        self.session.commit()
        return self.output(event)

    def update(self, id, category_id, sub_category_id,
               start_place_id=None, end_place_id=None, **kwargs):
        event = self.session.query(Event).filter(
            Event.id == id
        ).first()

        if not event:
            raise BadRequestParams('EventNotFound')

        if event.itinerary.creator_id != self.auth_user.id:
            raise PermissionError

        category = self.session.query(EventCategory).filter(
            EventCategory.id == category_id
        ).first()
        if not category:
            raise BadRequestParams('InvalidCategoryId')

        if sub_category_id != event.sub_category_id:
            sub_cate = self.session.query(EventSubCategory).filter(
                EventSubCategory.category_id == category_id,
                EventSubCategory.id == sub_category_id
            ).first()
            if not sub_cate:
                raise BadRequestParams('InvalidCategoryItemId')

            event.category_id = category_id
            event.sub_category_id = sub_category_id

        if start_place_id:
            s_place = self.session.query(Place).get(start_place_id)
            if not s_place:
                raise BadRequestParams('PlaceNotFound')
        event.start_place_id = start_place_id

        if category.code == 'travel':
            if end_place_id:
                e_place = self.session.query(Place).get(end_place_id)
                if not e_place:
                    raise BadRequestParams('PlaceNotFound')
            event.end_place_id = end_place_id
        else:
            event.end_place_id = None
        event = Event.convert_from_dict(data=kwargs, origin=event)
        self.session.add(event)
        self.session.commit()

        return self.output(event)

    def list(self, page, per_page, order, day_id, **kwargs):
        day = self.session.query(Day).filter(
            Day.id == day_id
        ).first()

        if not day:
            raise BadRequestParams('DayNotFound')

        output_extra_params = dict()

        if day.itinerary.status == 'private':
            if not self.auth_user:
                raise PermissionError

            share = get_itinerary_share(self.session, day.itinerary, self.auth_user)
            if day.itinerary.creator_id != self.auth_user.id and not share:
                raise PermissionError
            output_extra_params['share'] = share

        matches = self.session.query(Event)

        matches = matches.filter(
            Event.day_id == day_id
        )

        matches = sort_query(order, Event, matches)
        total = matches.count()
        result = []
        for e in paginate(matches, page, per_page):
            result.append(self.output(e, **output_extra_params))

        return dict(total=total, result=result)

    def delete(self, ids, day_id):
        day = self.session.query(Day).filter(
            Day.id == day_id
        ).first()
        if not day:
            raise BadRequestParams('DayNotFound')
        if day.itinerary.creator_id != self.auth_user.id:
            raise PermissionError

        for id in ids:
            e = self.session.query(Event).filter(
                Event.id == id,
                Event.day_id == day_id
            ).first()

            if not e:
                continue

            # delete all images
            self.session.query(Image).filter(
                Image.object_type == 'event',
                Image.object_id == id
            ).delete()

            # delete all attachments
            self.session.query(Attachment).filter(
                Attachment.object_type == 'event',
                Attachment.object_id == id
            ).delete()

            # delete all comments
            self.session.query(Comment).filter(
                Comment.object_type == 'event',
                Comment.object_id == id
            ).delete()

            # todo: push message to queue for cleaning google storage files
            self.session.delete(e)
        self.session.commit()
        return dict(ids=ids)

    def move(self, id, day_id):
        event = self.session.query(Event).get(id)
        if not event:
            raise BadRequestParams('EventNotFound')

        if event.itinerary.creator_id != self.auth_user.id:
            raise PermissionError

        day = self.session.query(Day).get(day_id)
        if not day:
            raise BadRequestParams('DayNotFound')

        if day.itinerary.creator_id != self.auth_user.id:
            raise PermissionError

        if day.itinerary.id != event.itinerary.id:
            raise BadRequestParams('MismatchItinerary',
                                   message='Event and day must be in the same itinerary.')
        event.day_id = day_id
        self.session.add(event)
        self.session.commit()
        return self.output(event)
