import os
from slugify import slugify

from tripary.databases.postgres import Event, Attachment
from tripary.api.factory.base_logic import BaseLogic
from tripary.api.factory.base_error import BadRequestParams, ServerError, PermissionError
from tripary.clients import google_storage_client as gs_client
from tripary.clients.google_storage import errors as gs_errors
from tripary.common.constants import APP_ROOT_DIR, MAX_ATTACHMENT_COUNT
from tripary.common.utils import sort_query, paginate
from tripary.api.factory.utils import get_itinerary_share


OBJECT_TYPE_MAP = {
    'event': Event
}


class AttachmentBL(BaseLogic):

    def _handle_path(self, obj, object_type):
        if object_type == 'event':
            return [obj.itinerary_id, obj.day_id, obj.id]
        if object_type == 'itinerary':
            return [obj.id]

    def _get_obj(self, object_type, object_id):
        try:
            table = OBJECT_TYPE_MAP[object_type]
        except KeyError:
            raise BadRequestParams('UnsupportedObjectType')
        obj = self.session.query(table).get(object_id)
        if not obj:
            raise BadRequestParams('ObjectNotFound')

        return obj

    def create(self, object_type, object_id, file):
        obj = self._get_obj(object_type, object_id)

        self._check_permission(object_type, object_id)

        current_file_count = self.session.query(Attachment).filter(
            Attachment.object_id == object_id,
            Attachment.object_type == object_type,
        ).count()
        if current_file_count >= MAX_ATTACHMENT_COUNT:
            raise BadRequestParams('ReachMaximumFile')

        attachment = Attachment()
        attachment.object_type = object_type
        attachment.object_id = object_id
        attachment.creator_id = self.auth_user.id
        attachment.name = file.name
        attachment.slug = slugify(file.name)
        attachment.content_type = file.type

        path = self._handle_path(obj, object_type)

        tmp_dir = os.path.join(APP_ROOT_DIR, 'tmp')
        if not os.path.exists(tmp_dir):
            os.makedirs(tmp_dir)

        tmp_path = os.path.join(tmp_dir, slugify(attachment.name))
        with open(tmp_path, 'wb') as wb_file:
            wb_file.write(file.body)

        try:
            upload_data = gs_client.upload(file_type='attachment',
                                           file_name=attachment.name,
                                           file_path=tmp_path,
                                           path=path)
        except (gs_errors.ContentTypeNotSupported,
                gs_errors.FileTypeNotSupported) as e:
            raise BadRequestParams(e.__class__.__name__)
        except Exception as e:
            raise ServerError(e)

        os.remove(tmp_path)

        attachment.size = upload_data['size']
        attachment.url = upload_data['url']
        attachment.path = upload_data['path']

        self.session.add(attachment)
        self.session.commit()
        return attachment.output()

    def delete(self, ids):
        for id in ids:
            attachment = self.session.query(Attachment).get(id)
            if not attachment:
                continue

            self._check_permission(attachment.object_type, attachment.object_id)

            self.session.delete(attachment)

        self.session.commit()
        return dict(ids=ids)

    def _check_permission(self, object_type, object_id):
        pass

    def update(self, id, public=None, **kwargs):
        att = self.session.query(Attachment).get(id)
        if not att:
            raise BadRequestParams('AttachmentNotFound')
        if att.creator_id != self.auth_user.id:
            raise PermissionError

        if public is not None:
            att.public = public
        self.session.add(att)
        self.session.commit()
        return att.output()

    def list(self, page, per_page, order, object_id, object_type, **kwargs):
        obj = self._get_obj(object_type, object_id)

        matches = self.session.query(Attachment).filter(
            Attachment.object_id == obj.id,
            Attachment.object_type == object_type,
        )

        if self.auth_user:
            if object_type == 'event':
                is_shared = get_itinerary_share(self.session, obj.itinerary, self.auth_user)
                if not is_shared and self.auth_user.id != obj.itinerary.creator_id:
                    matches = matches.filter(Attachment.public.is_(True))
        else:
            matches = matches.filter(
                Attachment.public.is_(True)
            )

        matches = sort_query(order, Attachment, matches)

        total = matches.count()
        result = [i.output() for i in paginate(matches, page, per_page)]
        return dict(total=total, result=result)
