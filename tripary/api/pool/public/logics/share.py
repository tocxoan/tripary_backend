import asyncio
from slugify import slugify
from sqlalchemy import or_

from tripary.databases.postgres import User, Share, Itinerary, Group
from tripary.api.factory.base_logic import BaseLogic
from tripary.common.utils import sort_query, paginate
from tripary.api.factory.base_error import BadRequestParams, PermissionError
from tripary.clients.mail_gunner import mail_gunner


class ShareBL(BaseLogic):

    def list(self, page, per_page, order,
             itinerary_id, search_text=None, **kwargs):

        it = self.session.query(Itinerary).filter(
            Itinerary.id == itinerary_id
        ).first()
        if not it:
            raise BadRequestParams('ItineraryNotFound')
        if it.creator_id != self.auth_user.id:
            raise PermissionError

        matches = self.session.query(Share).filter(
            Share.itinerary_id == itinerary_id
        )
        if search_text:
            pass

        matches = sort_query(order, Share, matches)
        return dict(
            total=matches.count(),
            result=[u.output() for u in paginate(matches, page, per_page)]
        )

    def create(self, itinerary_id, user_ids=None, group_ids=None):
        it = self.session.query(Itinerary).filter(
            Itinerary.id == itinerary_id
        ).first()
        if not it:
            raise BadRequestParams('ItineraryNotFound')

        if it.creator_id != self.auth_user.id:
            raise PermissionError

        result = []

        if not user_ids:
            user_ids = []
        if not group_ids:
            group_ids = []

        for user_id in user_ids:
            share = Share()
            share.itinerary_id = itinerary_id
            if user_id == self.auth_user.id:
                raise BadRequestParams(message='Cannot share to yourself')

            user = self.session.query(User).filter(
                User.id == user_id
            )
            if not user:
                raise BadRequestParams('UserNotFound')

            duplication = self.session.query(Share).filter(
                Share.user_id == user_id,
                Share.itinerary_id == itinerary_id
            ).first()
            if duplication:
                raise BadRequestParams('DuplicateShare')

            share.user_id = user_id

            self.session.add(share)

            result.append(share)

        for group_id in group_ids:
            share = Share()
            share.itinerary_id = itinerary_id

            group = self.session.query(Group).filter(
                Group.id == group_id
            ).first()
            if not group:
                raise BadRequestParams('GroupNotFound')

            duplication = self.session.query(Share).filter(
                Share.group_id == group_id,
                Share.itinerary_id == itinerary_id
            ).first()

            if duplication:
                raise BadRequestParams('DuplicateShare')

            share.group_id = group_id

            self.session.add(share)

            result.append(share)

        self.session.commit()

        share_ids = [s.id for s in result]

        asyncio.ensure_future(self.handle_after_sharing(share_ids, it_id=it.id))

        return [s.output() for s in result]

    def delete(self, ids, itinerary_id):
        it = self.session.query(Itinerary).filter(
            Itinerary.id == itinerary_id
        ).first()
        if it.creator_id != self.auth_user.id:
            raise PermissionError

        if not it:
            raise BadRequestParams('ItineraryNotFound')

        for id in ids:
            share = self.session.query(Share).filter(
                Share.id == id
            ).first()
            if not share:
                raise BadRequestParams('ShareNotFound')

            self.session.delete(share)
        self.session.commit()
        return dict(ids=ids)

    def handle_share_url(self, itinerary_id):
        it = self.session.query(Itinerary).get(itinerary_id)
        if not it:
            raise BadRequestParams('ItineraryNotFound')
        share = self.session.query(Share).filter(
            Share.itinerary_id == itinerary_id,
            Share.user_id == self.auth_user.id
        ).first()
        if not share:
            share = Share()
            share.itinerary_id = itinerary_id
            share.user_id = self.auth_user.id
            self.session.add(share)
        self.session.commit()
        return share.output()

    async def handle_after_sharing(self, share_ids,
                                   it_id):

        it = self.session.query(Itinerary).get(it_id)

        direct_shared_emails = set()
        shared_groups = set()
        for id in share_ids:
            share = self.session.query(Share).get(id)
            if share.user:
                direct_shared_emails.add(share.user.email)

            else:
                shared_groups.add(share.group)

        email_template = self.templates.get_template('mail_content/itinerary_share.html')
        redirect_url = self.app.config['WEB_DOMAIN'] + '/share/' + it_id

        if direct_shared_emails:
            subject = '%s đã chia sẻ cho bạn lịch trình - %s' % (it.creator.email, it.name)
            mail_gunner.send(to_addr=list(direct_shared_emails),
                             subject=subject,
                             body=email_template.render(
                                 share_type='direct',
                                 it=it,
                                 redirect_url=redirect_url
                             ))

        for group in shared_groups:
            subject = '%s đã chia sẻ vào nhóm %s lịch trình - %s' % (
                it.creator.name or it.creator.email,
                group.name,
                it.name
            )
            to_addrs = list(filter(lambda x: x.id != it.creator_id, group.members))
            to_addrs = list(map(lambda x: x.email, to_addrs))
            mail_gunner.send(to_addr=to_addrs,
                             subject=subject,
                             body=email_template.render(
                                 share_type='group',
                                 it=it,
                                 redirect_url=redirect_url,
                                 group=group
                             ))
