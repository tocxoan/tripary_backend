import inspect
from sanic.views import HTTPMethodView, HTTP_METHODS
from sanic import response

# from promotion.models.role import Role
from tripary.databases.postgres import User
from tripary.common.utils import decode_jwt_token
from tripary.common.constants import DEFAULT_METHODS_PARAM_LOCATION
from tripary.common.schemas import BaseGettingSchema
from tripary.msg_senders import sender_manager

from .base_logic import BaseLogic
from .base_error import *


class BaseResource(HTTPMethodView):
    auth_required = False

    GET = dict(
        schema=BaseGettingSchema,
        param_location='args',
        auth_required=False,
        logic_func='get',
        has_file=False,
    )

    logic_class = BaseLogic

    def __init__(self):
        for http_method in HTTP_METHODS:
            async def handle_request(request):
                method_opts = getattr(self, request.method, None)
                if not method_opts:
                    raise MethodNotAllowed

                # get sql_session and sql
                sql_session = request['sql_session']
                sql = request['sql']

                '''Handle authentication'''
                auth_user = self.handle_auth(request)
                auth_required = self.auth_required or method_opts.get('auth_required')
                if auth_required and not auth_user:
                    raise Unauthorized

                '''Get user's role'''
                # role_id = getattr(auth_user, 'role_id', None)
                # if role_id:
                #     role = Role.filter(id=role_id).first()
                # else:
                #     role = None

                '''Handle input data'''
                params = self.parse_request_params(request, method_opts)

                # get logic function
                logic = self.logic_class(session=sql_session,
                                         app=request.app,
                                         sm=sender_manager,
                                         sql=sql,
                                         auth_user=auth_user)
                logic_func_name = method_opts.get('logic_func', None)
                logic_func = getattr(logic, logic_func_name, None)
                if not logic_func:
                    raise NotImplementedError('%s does not have function %s' % (self.logic_class.__name__,
                                                                                logic_func_name))

                # do logic
                if inspect.iscoroutinefunction(logic_func):
                    result = await logic_func(**params)
                else:
                    result = logic_func(**params)
                return response.json(result)

            handle_request.__name__ = http_method.lower()
            setattr(self, http_method.lower(), handle_request)

    def parse_request_params(self, request, method_opts):
        method = request.method

        input_schema = method_opts.get('schema')
        if not input_schema:
            return {}
        input_schema = input_schema()
        param_location = method_opts.get('param_location',
                                         DEFAULT_METHODS_PARAM_LOCATION.get(method.lower()))
        if not param_location:
            raise NotImplementedError('Missing %s param location!' % method)

        param_container = getattr(request, param_location) or {}
        raw_params = {}
        for key in param_container:
            raw_params[key] = param_container.get(key)

        if method_opts.get('has_file'):
            file = request.files.get('file')
            if not file:
                raise BadRequestParams('Missing file!')
            raw_params['file'] = file

        err = input_schema.validate(raw_params)
        if err:
            raise BadRequestParams(message=str(err))

        return input_schema.load(raw_params).data

    def handle_auth(self, request):
        auth_data = request.headers.get('Authorization', None)
        if not auth_data:
            return None

        try:
            bearer, access_token = auth_data.split(' ')
        except ValueError:
            return None

        if bearer != 'Bearer' or not access_token:
            return None

        token_data = decode_jwt_token(token=access_token,
                                      secret_key=request.app.config['SECRET_KEY'])
        if not token_data:
            return None

        try:
            user_id = token_data['id']
        except Exception as error:
            print(error)
            return None

        sql_session = request['sql_session']

        user = sql_session.query(User).filter(
            User.id == user_id
        ).first()
        if not user:
            return None

        return user


__all__ = (
    'BaseResource'
)
