from sanic import Sanic
from sanic.response import json
from sanic_cors import CORS
from sanic.exceptions import SanicException


class Factory(object):
    def __init__(self, config,
                 sql_db,
                 resources=None,
                 app_name=None,
                 error_handler=None,
                 request_callback=None,
                 response_callback=None):

        self.config = config
        self.sql_db = sql_db
        self.resources = resources or {}
        self.app_name = app_name or 'Sanic'
        self.error_handler = error_handler
        self.request_callback = request_callback
        self.response_callback = response_callback

    @staticmethod
    def handle_error(request, error):
        if isinstance(error, SanicException):
            status_code = error.status_code
            data = error
        else:
            status_code = 500
            if request.app.config.DEBUG:
                raise error
            data = dict(message='Server error - %s' % str(error))
        return json(data, status_code)

    def create_app(self):
        app = Sanic(name=self.app_name)

        '''Cross origin'''
        CORS(app, supports_credentials=True,
             automatic_options=True)

        '''Load config'''
        app.config.from_object(self.config)

        '''Error handling configuration'''
        error_handler = self.error_handler or self.handle_error
        app.error_handler.add(Exception, error_handler)

        @app.middleware('request')
        # Callback before starting request
        def handle_before_request(request):
            # start sql session
            request['sql_session'] = self.sql_db.start_session()
            request['sql'] = self.sql_db
            # hook callback
            if self.request_callback:
                self.request_callback(request)

        @app.middleware('response')
        # Callback after returning response
        def handle_after_request(request, response):

            # hook callback
            if self.response_callback:
                self.response_callback(request, response)

            # close and delete sql_session
            session = request.pop('sql_session', None)
            if session:
                session.close()

        '''Resources installation'''
        for endpoint, resource in self.resources.items():
            app.add_route(resource.as_view(), endpoint)

        return app
