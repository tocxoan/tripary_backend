from sqlalchemy import or_, desc

from tripary.databases.postgres import *


def get_itinerary_share(session, itinerary, user):
    share = session.query(Share).outerjoin(
        GroupMember,
        GroupMember.group_id == Share.group_id
    ).filter(
        or_(
            Share.user_id == user.id,
            GroupMember.user_id == user.id
        ),
        Share.itinerary_id == itinerary.id
    ).first()
    return share


def sort_query(query, table, sorts):
    sort_list = []

    for string in sorts:
        # get key, value of sort item
        try:
            key, value = string.split('==')
        except ValueError:
            key = string
            value = None

        # get column name and descending
        if key.startswith('-'):
            field_name = key[1:len(key)]
            descending = True
        else:
            descending = False
            if string.startswith('+'):
                field_name = key[1:len(key)]
            else:
                field_name = key

        # get sqlalchemy sort object
        field = getattr(table, field_name, None)
        if field:
            if value:
                sort = field == value
            else:
                sort = field

            if descending:
                sort = desc(sort)
            sort_list.append(sort)

    if sort_list:
        return query.order_by(*sort_list)

    return query
