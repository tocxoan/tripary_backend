from jinja2 import Environment, FileSystemLoader, select_autoescape

from tripary.common.constants import TEMPLATE_DIR


class BaseLogic(object):
    def __init__(self, app, session, sm, sql, auth_user=None, role=None):
        self.app = app
        self.session = session
        self.sql = sql
        self.auth_user = auth_user
        self.role = role
        self.sm = sm
        self.templates = Environment(
            loader=FileSystemLoader(TEMPLATE_DIR),
            autoescape=select_autoescape(['html', 'xml'])
        )

    def _get(self, id, table):
        return self.session.query(table).filter(
            table.deleted.is_(False),
            table.id == id
        ).first()
