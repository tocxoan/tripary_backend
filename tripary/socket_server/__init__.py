import socketio
from socketio import AsyncRedisManager
from sanic import Sanic

from config import MessageQueueConfig

app = Sanic()
rm = AsyncRedisManager(*MessageQueueConfig.SOCKET)
sio = socketio.AsyncServer(async_mode='sanic',
                           client_manager=rm)
sio.attach(app)


@sio.on('connect')
def connect(sid, environ):
    print('connect ', sid)


@sio.on('join_room')
def join_room(sid, room):
    sio.enter_room(sid, room=room)


@sio.on('leave_room')
def leave_room(sid, room):
    sio.leave_room(sid, room=room)


@sio.on('disconnect')
def disconnect(sid):
    # leave all rooms when disconnected
    for room in sio.rooms(sid):
        sio.leave_room(sid, room=room)
    print('disconnected on, ', sid)
