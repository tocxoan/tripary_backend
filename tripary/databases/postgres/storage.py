from sqlalchemy import (Column, String, ForeignKey, Boolean)
from sqlalchemy.orm import relationship, backref

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel, BaseFile


class Image(BaseModel, BaseFile):
    desc = Column(String(STRING_LENGTH['LARGE']))
    object_type = Column(String(STRING_LENGTH['EX_SHORT']), nullable=False, index=True)
    object_id = Column(String(STRING_LENGTH['UUID4']), nullable=False, index=True)
    creator_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('user.id'),
                        nullable=False, index=True)

    is_default = Column(Boolean, default=False)

    creator = relationship('User',
                           backref=backref('images', cascade='all,delete'))


class Attachment(BaseModel, BaseFile):
    object_type = Column(String(STRING_LENGTH['EX_SHORT']), nullable=False, index=True)
    object_id = Column(String(STRING_LENGTH['UUID4']), nullable=False, index=True)
    creator_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('user.id'),
                        nullable=False, index=True)

    creator = relationship('User',
                           backref=backref('attachments', cascade='all,delete'))
