from sqlalchemy import (Column, String, ForeignKey, Integer)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import JSONB

from tripary.common.constants import STRING_LENGTH, DATETIME_FORMAT

from .base import BaseModel


class Comment(BaseModel):
    attachment_type = Column(String(STRING_LENGTH['EX_SHORT']),
                             index=True)
    creator_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('user.id'),
                        nullable=False, index=True)
    content = Column(String(STRING_LENGTH['LARGE']), nullable=False)
    parent_id = Column(String(STRING_LENGTH['UUID4']), index=True)
    object_id = Column(String(STRING_LENGTH['UUID4']),
                       nullable=False, index=True)
    object_type = Column(String(STRING_LENGTH['EX_SHORT']),
                         nullable=False, index=True)

    children_count = Column(Integer, default=0)
    like_count = Column(Integer, default=0)

    attachment = Column(JSONB, default={})

    creator = relationship('User', backref=backref('comments', cascade='all,delete'))

    def serialize_data(self):
        result = super().serialize_data()
        result['creator'] = self.creator.output(includes=['id', 'avatar_url', 'name'])
        result['created_at'] = self.created_at.strftime(DATETIME_FORMAT)
        return result
