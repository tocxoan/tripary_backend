from sqlalchemy import (Column, String)
from sqlalchemy.dialects.postgresql import JSONB
from geoalchemy2 import Geometry
from shapely import wkb

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel


class Place(BaseModel):
    map_id = Column(String(STRING_LENGTH['LONG']), index=True)
    name = Column(String(STRING_LENGTH['LONG']), index=True, nullable=False)
    address = Column(String(STRING_LENGTH['LARGE']))
    map_url = Column(String(STRING_LENGTH['LONG']))
    website = Column(String(STRING_LENGTH['LONG']))
    phone = Column(String(STRING_LENGTH['LONG']))
    place_types = Column(String(STRING_LENGTH['LONG']), index=True, nullable=False)

    coordinates = Column(Geometry('POINT'), index=True)

    address_components = Column(JSONB, default=[])
    viewport = Column(JSONB)
    images = Column(JSONB, default=[])

    def serialize_data(self):
        result = super().serialize_data()
        if self.coordinates is not None:
            coordinate = wkb.loads(bytes(self.coordinates.data))
            result['coordinates'] = {
                'lat': coordinate.y,
                'lng': coordinate.x
            }

        result['place_types'] = self.place_types.split(',')
        return result
