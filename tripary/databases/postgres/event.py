from sqlalchemy import (Column, String, ForeignKey, Integer)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import JSONB

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel


class EventCategory(BaseModel):
    code = Column(String(STRING_LENGTH['EX_SHORT']), nullable=False, index=True)
    name = Column(String(STRING_LENGTH['LONG']), nullable=False, index=True)
    icon = Column(String(STRING_LENGTH['SHORT']), nullable=False)
    color = Column(String(STRING_LENGTH['SHORT']), nullable=False)

    place_types = Column(JSONB, default=[])
    meta_fields = Column(JSONB, default=[])


class EventSubCategory(BaseModel):
    name = Column(String(STRING_LENGTH['LONG']), nullable=False, index=True)
    code = Column(String(STRING_LENGTH['EX_SHORT']), nullable=False, index=True)
    icon = Column(String(STRING_LENGTH['SHORT']))
    category_id = Column(String(STRING_LENGTH['UUID4']),
                         ForeignKey('event_category.id'),
                         nullable=False, index=True)

    place_types = Column(JSONB, default=[])
    meta_fields = Column(JSONB, default=[])

    category = relationship('EventCategory', backref=backref('sub_categories', cascade='all,delete'))


class Event(BaseModel):
    category_id = Column(String(STRING_LENGTH['UUID4']),
                         ForeignKey('event_category.id'),
                         nullable=False, index=True)
    sub_category_id = Column(String(STRING_LENGTH['UUID4']),
                             ForeignKey('event_sub_category.id'),
                             nullable=False, index=True)
    day_id = Column(String(STRING_LENGTH['UUID4']),
                    ForeignKey('day.id'), nullable=False, index=True)
    start_place_id = Column(String(STRING_LENGTH['UUID4']), ForeignKey('place.id'), index=True)
    end_place_id = Column(String(STRING_LENGTH['UUID4']), index=True)
    itinerary_id = Column(String(STRING_LENGTH['UUID4']),
                          ForeignKey('itinerary.id'),
                          nullable=False, index=True)
    currency = Column(String(STRING_LENGTH['EX_SHORT']), default='VND', index=True)
    name = Column(String(STRING_LENGTH['LONG']), index=True)
    start_time = Column(String(STRING_LENGTH['EX_SHORT']), default='00:00', index=True)
    end_time = Column(String(STRING_LENGTH['EX_SHORT']), index=True)
    note = Column(String(STRING_LENGTH['EX_LARGE']))

    price = Column(Integer, index=True, default=0)
    comment_count = Column(Integer, index=True, default=0)

    meta = Column(JSONB, default={})

    day = relationship('Day', backref=backref('events', cascade='all,delete'))
    category = relationship('EventCategory', backref=backref('events', cascade='all,delete'))
    sub_category = relationship('EventSubCategory', backref=backref('events', cascade='all,delete'))
    itinerary = relationship('Itinerary', backref=backref('events', cascade='all,delete'))
    start_place = relationship('Place', backref=backref('events', cascade='all,delete'))

    def serialize_data(self, **kwargs):
        result = super().serialize_data(**kwargs)
        result['category'] = self.category.output()
        result['sub_category'] = self.sub_category.output()

        if self.start_place:
            result['start_place'] = self.start_place.output()

        return result
