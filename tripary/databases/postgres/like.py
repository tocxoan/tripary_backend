from sqlalchemy import (Column, String, ForeignKey)
from sqlalchemy.orm import relationship, backref

from tripary.common.constants import STRING_LENGTH, DATETIME_FORMAT

from .base import BaseModel


class Like(BaseModel):
    creator_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('user.id'),
                        nullable=False, index=True)
    object_id = Column(String(STRING_LENGTH['UUID4']),
                       nullable=False, index=True)
    object_type = Column(String(STRING_LENGTH['EX_SHORT']),
                         nullable=False, index=True)

    creator = relationship('User', backref=backref('likes', cascade='all,delete'))

    def serialize_data(self):
        result = super().serialize_data()
        result['creator'] = self.creator.output(includes=['avatar_url', 'name', 'email'])
        result['created_at'] = self.created_at.strftime(DATETIME_FORMAT)
        return result
