from datetime import timedelta
from copy import deepcopy
from sqlalchemy import (Column, String, ForeignKey, DateTime, Boolean, select, func, text)
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import JSONB

from tripary.common.constants import STRING_LENGTH, DEFAULT_ITINERARY_META

from .base import BaseModel
from .day import Day


class Itinerary(BaseModel):
    name = Column(String(STRING_LENGTH['LONG']), nullable=False, index=True)
    status = Column(String(STRING_LENGTH['EX_SHORT']), default='private', index=True)
    desc = Column(String(STRING_LENGTH['LARGE']))
    creator_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('user.id'),
                        nullable=False, index=True)

    start_date = Column(DateTime, nullable=False, index=True)

    is_draft = Column(Boolean, default=True, index=True)

    meta = Column(JSONB, default=deepcopy(DEFAULT_ITINERARY_META))

    creator = relationship('User', backref=backref('itineraries', cascade='all,delete'))

    # for migration
    old_id = Column(String(STRING_LENGTH['LONG']))

    @hybrid_property
    def day_count(self):
        return len(getattr(self, 'days', []))

    @day_count.expression
    def day_count(self):
        return select([func.count(Day.id)]).where(Day.itinerary_id == self.id).label('day_count')

    @hybrid_property
    def end_date(self):
        return self.start_date + timedelta(days=self.day_count)

    @end_date.expression
    def end_date(self):
        return (self.start_date + self.day_count * text("interval '1' day")).label('end_date')

    def serialize_data(self, **kwargs):
        result = super().serialize_data(**kwargs)
        result['creator'] = self.creator.output(includes=['name', 'avatar_url'])
        result['shares'] = [share.output() for share in getattr(self, 'shares', [])]
        return result
