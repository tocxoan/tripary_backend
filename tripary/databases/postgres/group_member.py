from sqlalchemy import (Column, String, ForeignKey)
from sqlalchemy.orm import relationship, backref

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel


class GroupMember(BaseModel):
    user_id = Column(String(STRING_LENGTH['UUID4']),
                     ForeignKey('user.id'),
                     nullable=False, index=True)
    status = Column(String(STRING_LENGTH['EX_SHORT']),
                    default='active', index=True)
    group_id = Column(String(STRING_LENGTH['UUID4']),
                      ForeignKey('group.id'),
                      nullable=False, index=True)
    group = relationship('Group', backref=backref('members', cascade='all,delete'))
    user = relationship('User', backref=backref('group_members', cascade='all,delete'))

    def serialize_data(self):
        result = super().serialize_data()
        result['user'] = self.user.output(includes=['name', 'avatar_url', 'email'])
        return result
