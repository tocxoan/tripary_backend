from sqlalchemy import (Column, String, ForeignKey, DateTime, select, func)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy.ext.hybrid import hybrid_property

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel


class Notification(BaseModel):
    user_id = Column(String(STRING_LENGTH['UUID4']),
                     ForeignKey('user.id'),
                     index=True, nullable=False)
    action_type_code = Column(String(STRING_LENGTH['EX_SHORT']),
                              ForeignKey('notification_action_type.code'),
                              index=True, nullable=False)
    status = Column(String(STRING_LENGTH['EX_SHORT']),
                    index=True, default='created')
    object_type = Column(String(STRING_LENGTH['EX_SHORT']),
                         index=True, nullable=False)
    object_id = Column(String(STRING_LENGTH['UUID4']),
                       index=True, nullable=False)
    avatar_url = Column(String(STRING_LENGTH['LONG']))

    event_time = Column(DateTime, nullable=False, index=True)
    sent_at = Column(DateTime, index=True)
    seen_at = Column(DateTime, index=True)
    read_at = Column(DateTime, index=True)

    user = relationship('User',
                        backref=backref('notifications', cascade='all,delete'))
    action_type = relationship('NotificationActionType',
                               backref=backref('notifications', cascade='all,delete'))

    @property
    def content(self):
        result = self.action_type.content_template
        if self.action_type_code in ['comment', 'reply-comment']:
            event_creators = getattr(self, 'event_creators', [])
            event_creators.sort(key=lambda x: x.event_time, reverse=True)
            last_event_creator = event_creators[0]
            comment = last_event_creator.data['comment']

            subject = '<b>%s</b>' % comment['creator']['name']
            if len(event_creators) > 2:
                subject += ' ' + 'và %s người khác' % (len(event_creators) - 1)

            if comment['object_type'] == 'event':
                obj = 'một sự kiện'
                obj += ' ' + 'trong lịch trình <b>%s</b>' % last_event_creator.data['itinerary']['name']

                obj_owner = last_event_creator.data['itinerary']['creator']
                if obj_owner['id'] == self.user_id:
                    complement = 'của bạn.'
                else:
                    complement = 'của <b>%s</b>' % obj_owner['name']
            else:
                obj = ''
                complement = ''

            if self.action_type_code == 'comment':
                result = result.format(subject, obj, complement)
            else:
                result = result.format(subject, 'của bạn', obj)
        return result

    @hybrid_property
    def event_creator_count(self):
        return len(getattr(self, 'event_creators', []))

    @event_creator_count.expression
    def event_creator_count(self):
        return select([func.count(NotificationEventCreator.id)]).where(
            NotificationEventCreator.notification_id == self.id).label('event_creator_count')

    @hybrid_property
    def last_event_creator(self):
        event_creators = getattr(self, 'event_creators', [])
        event_creators.sort(key=lambda x: x.created_at, reverse=True)
        last_event_creator = event_creators[0]
        return last_event_creator.output()

    def serialize_data(self):
        result = super().serialize_data()
        result['content'] = self.content
        return result


class NotificationEventCreator(BaseModel):
    creator_type = Column(String(STRING_LENGTH['EX_SHORT']),
                          index=True, nullable=False)
    creator_id = Column(String(STRING_LENGTH['UUID4']),
                        index=True, nullable=False)
    notification_id = Column(String(STRING_LENGTH['UUID4']),
                             ForeignKey('notification.id'),
                             index=True, nullable=False)
    avatar_url = Column(String(STRING_LENGTH['LONG']))

    data = Column(JSONB, default={})

    event_time = Column(DateTime, nullable=False, index=True)

    notification = relationship('Notification',
                                backref=backref('event_creators', cascade='all,delete'))


class NotificationActionType(BaseModel):
    code = Column(String(STRING_LENGTH['EX_SHORT']),
                  index=True, nullable=False, unique=True)
    content_template = Column(String(STRING_LENGTH['LARGE']), nullable=False)
