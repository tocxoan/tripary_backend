from sqlalchemy import (Column, String, ForeignKey)
from sqlalchemy.orm import relationship, backref

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel, Person


class FacebookInfo(BaseModel, Person):
    uid = Column(String(STRING_LENGTH['LONG']), index=True, nullable=False)
    access_token = Column(String(STRING_LENGTH['LARGE']), index=True, nullable=False)
    avatar_url = Column(String(STRING_LENGTH['LARGE']))
    user_id = Column(String(STRING_LENGTH['UUID4']), ForeignKey('user.id'),
                     nullable=False, index=True)

    user = relationship('User', backref=backref('facebook_info', uselist=False))


class GoogleInfo(BaseModel, Person):
    uid = Column(String(STRING_LENGTH['LONG']), index=True, nullable=False)
    token = Column(String(STRING_LENGTH['LARGE']), index=True, nullable=False)

    avatar_url = Column(String(STRING_LENGTH['LARGE']))
    user_id = Column(String(STRING_LENGTH['UUID4']), ForeignKey('user.id'),
                     nullable=False, index=True)

    user = relationship('User', backref=backref('google_info', uselist=False))


class User(BaseModel, Person):
    status = Column(String(STRING_LENGTH['EX_SHORT']),
                    default='active', index=True)
    avatar_url = Column(String(STRING_LENGTH['LARGE']))

    # for migration
    old_id = Column(String(STRING_LENGTH['LONG']))


class Invitation(BaseModel):
    user_id = Column(String(STRING_LENGTH['UUID4']),
                     ForeignKey('user.id'),
                     nullable=False, index=True)
    creator_id = Column(String(STRING_LENGTH['UUID4']),
                        nullable=False, index=True)
    object_id = Column(String(STRING_LENGTH['UUID4']),
                       nullable=False, index=True)
    object_type = Column(String(STRING_LENGTH['EX_SHORT']),
                         nullable=False, index=True)
    status = Column(String(STRING_LENGTH['EX_SHORT']),
                    default='pendding', index=True)
    user = relationship('User', backref=backref('invitations', cascade='all,delete'))
