import uuid
from datetime import datetime
from sqlalchemy.ext.declarative import declarative_base, declared_attr
from sqlalchemy import Column, String, DateTime, Boolean, Integer
from sqlalchemy.ext.compiler import compiles
from sqlalchemy.ext.hybrid import hybrid_property
from sqlalchemy.sql import expression
from sqlalchemy.inspection import inspect as sa_inspect

from tripary.common.constants import STRING_LENGTH, DATE_FORMAT

Base = declarative_base()


class UtcNow(expression.FunctionElement):
    type = DateTime()


@compiles(UtcNow, 'postgresql')
def get_now(element, compiler, **kw):
    return "TIMEZONE('utc', CURRENT_TIMESTAMP)"


class Serializer(object):
    def parse_value(self, value):
        if value is None:
            return None
        if isinstance(value, datetime):
            return value.strftime(DATE_FORMAT)
        return value

    def filter_data(self, data, includes=None, excludes=None, **kwargs):
        all_keys = set(data.keys())

        # if both includes and excludes are passed,
        # includes is higher priority
        if includes:
            result_keys = all_keys.intersection(set(includes or []))
        else:
            result_keys = all_keys.difference(set(excludes or []))

        must_includes = ['id']

        for must_include_key in must_includes:
            if must_include_key in result_keys:
                continue
            result_keys.add(must_include_key)

        all_columns = self.__table__.columns

        for key in all_keys:
            column = all_columns.get(key)
            if column is not None and column.primary_key:
                continue

            if key not in result_keys:
                data.pop(key, None)
        return data

    def serialize_data(self, **kwargs):
        result = dict()

        all_columns = self.__table__.columns
        for field_name in all_columns.keys():
            raw_value = getattr(self, field_name, None)
            result[field_name] = self.parse_value(raw_value)

        for attr in sa_inspect(self.__class__).all_orm_descriptors:
            if not isinstance(attr, hybrid_property):
                continue
            raw_value = getattr(self, attr.__name__, None)
            result[attr.__name__] = self.parse_value(raw_value)

        return result

    def fetch_translation(self, data, locale=None):
        translations = getattr(self, 'translations', [])

        # fetch available translation list
        data['translations'] = [tran.output(includes=['locale', 'name']) for tran in translations]

        if locale:
            try:
                matched_translation = list(filter(lambda i: i.locale == locale, translations))[0]
            except IndexError:
                matched_translation = None
            if not matched_translation:
                return data

            trans_output = matched_translation.output()
            for k, v in trans_output.items():
                if k in data and k != 'slug':
                    continue
                data[k] = v
        return data

    def output(self,
               includes=None,
               excludes=None,
               **kwargs):

        data = self.serialize_data(**kwargs)

        result = self.filter_data(data, includes, excludes)

        return result


class BaseModel(Base, Serializer):
    __abstract__ = True

    @declared_attr
    def __tablename__(cls):
        return ''.join('_%s' % c if c.isupper() else c
                       for c in cls.__name__).strip('_').lower()

    id = Column(String(STRING_LENGTH['UUID4']), primary_key=True, default=lambda: str(uuid.uuid4()))
    slug = Column(String(STRING_LENGTH['LONG']), index=True)

    created_at = Column(DateTime,
                        server_default=UtcNow(),
                        index=True)
    updated_at = Column(DateTime,
                        onupdate=datetime.utcnow,
                        index=True)

    deleted = Column(Boolean, default=False, index=True)
    deletable = Column(Boolean, default=True, index=True)
    editable = Column(Boolean, default=True, index=True)

    @classmethod
    def convert_from_dict(cls, data, origin=None):
        if origin:
            if not isinstance(origin, cls):
                raise Exception('Invalid origin object of %s.' % cls.__name__)
            result = origin
        else:
            result = cls()

        all_fields = cls.__table__.columns
        for field_name in all_fields.keys():
            value = data.get(field_name)
            if value is None:
                continue
            if value == getattr(result, field_name, None):
                continue
            setattr(result, field_name, value)

        return result

    def clone(self, excludes=None):
        _excludes = ['id', 'created_at', 'updated_at'] + excludes or []

        result = self.__class__()
        fields = self.__table__.columns
        for field_name in fields.keys():
            if field_name in _excludes:
                continue

            value = getattr(self, field_name, None)
            if value is None:
                continue

            setattr(result, field_name, value)
        return result


class Person(object):
    name = Column(String(STRING_LENGTH['LONG']), index=True)
    email = Column(String(STRING_LENGTH['LONG']), index=True)
    address = Column(String(STRING_LENGTH['LONG']))
    gender = Column(String(STRING_LENGTH['EX_SHORT']), index=True)
    phone = Column(String(STRING_LENGTH['EX_SHORT']), index=True)


class BaseFile(object):
    content_type = Column(String(STRING_LENGTH['LONG']), nullable=False, index=True)
    name = Column(String(STRING_LENGTH['LONG']), nullable=False, index=True)
    path = Column(String(STRING_LENGTH['LONG']), nullable=False)
    url = Column(String(STRING_LENGTH['LONG']), nullable=False)

    public = Column(Boolean, default=True, index=True)

    size = Column(Integer, default=0, index=True)


class BaseTranslation(object):
    locale = Column(String(STRING_LENGTH['EX_SHORT']),
                    nullable=False, index=True)
    name = Column(String(STRING_LENGTH['LONG']), index=True, nullable=False)
    description = Column(String(STRING_LENGTH['EX_LARGE']))
    content = Column(String(STRING_LENGTH['EX_LARGE']))
