from sqlalchemy import (Column, String, ForeignKey)

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel


class Group(BaseModel):
    name = Column(String(STRING_LENGTH['LONG']), index=True, nullable=False)
    creator_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('user.id'),
                        nullable=False, index=True)

    def serialize_data(self, fetch_member=False, **kwargs):
        result = super().serialize_data(**kwargs)
        result['member_count'] = len(getattr(self, 'members', []))
        if fetch_member:
            members = getattr(self, 'members', [])
            result['members'] = [mem.output() for mem in members]
        return result
