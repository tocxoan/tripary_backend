from sqlalchemy import (Column, String, ForeignKey)
from sqlalchemy.orm import relationship, backref
from sqlalchemy.dialects.postgresql import JSONB

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel


class Message(BaseModel):
    content_type = Column(String(STRING_LENGTH['EX_SHORT']),
                          index=True, nullable=False)
    creator_id = Column(String(STRING_LENGTH['UUID4']),
                        ForeignKey('user.id'),
                        nullable=False, index=True)
    object_id = Column(String(STRING_LENGTH['UUID4']),
                       nullable=False, index=True)
    object_type = Column(String(STRING_LENGTH['EX_SHORT']),
                         nullable=False, index=True)

    content = Column(JSONB, default={})

    creator = relationship('User', backref=backref('groups', cascade='all,delete'))

    def serialize_data(self):
        result = super().serialize_data()
        result['creator'] = self.creator.output(includes=['avatar_url', 'name'])
        return result
