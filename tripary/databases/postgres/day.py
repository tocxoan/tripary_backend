from sqlalchemy import (Column, String, Integer, ForeignKey)
from sqlalchemy.orm import relationship, backref

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel


class Day(BaseModel):
    name = Column(String(STRING_LENGTH['LONG']), index=True)
    itinerary_id = Column(String(STRING_LENGTH['UUID4']),
                          ForeignKey('itinerary.id'),
                          nullable=False, index=True)

    itinerary = relationship('Itinerary', backref=backref('days', cascade='all,delete'))

    def serialize_data(self):
        result = super().serialize_data()
        events = []
        for e in getattr(self, 'events', []):
            events.append(e.output())
        result['events'] = events
        return result
