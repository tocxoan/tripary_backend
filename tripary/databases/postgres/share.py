from sqlalchemy import (Column, String, ForeignKey)
from sqlalchemy.orm import relationship, backref

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel


class Share(BaseModel):
    user_id = Column(String(STRING_LENGTH['UUID4']),
                     ForeignKey('user.id'), index=True)
    group_id = Column(String(STRING_LENGTH['UUID4']),
                      ForeignKey('group.id'), index=True)
    status = Column(String(STRING_LENGTH['EX_SHORT']),
                    default='active', index=True)
    itinerary_id = Column(String(STRING_LENGTH['UUID4']),
                          ForeignKey('itinerary.id'),
                          index=True, nullable=False)
    user = relationship('User', backref=backref('shares', cascade='all,delete'))
    group = relationship('Group', backref=backref('shares', cascade='all,delete'))
    itinerary = relationship('Itinerary', backref=backref('shares', cascade='all,delete'))

    def serialize_data(self):
        result = super().serialize_data()
        if self.user:
            result['user'] = self.user.output(includes=['name', 'avatar_url', 'email'])
        if self.group:
            result['group'] = self.group.output()
        return result
