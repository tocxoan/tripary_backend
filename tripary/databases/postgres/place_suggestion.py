from sqlalchemy import (Column, String, ForeignKey, Integer)
from sqlalchemy.orm import relationship, backref

from tripary.common.constants import STRING_LENGTH

from .base import BaseModel


class PlaceSuggestion(BaseModel):
    event_id = Column(String(STRING_LENGTH['UUID4']),
                      ForeignKey('event.id'),
                      nullable=False, index=True)

    place_id = Column(String(STRING_LENGTH['UUID4']),
                      ForeignKey('place.id'),
                      nullable=False, index=True)

    like_count = Column(Integer, default=0)

    place = relationship('Place', backref=backref('place_suggestions', cascade='all,delete'))
    event = relationship('Event', backref=backref('place_suggestions', cascade='all,delete'))

    def serialize_data(self):
        result = super().serialize_data()
        result['place'] = self.place.output(includes=['name', 'address', 'map_id',
                                                      'coordinates',
                                                      'website', 'phone', 'images'])
        return result
