from api.common.base_models import BaseEmbeddedDocument, BaseDocument, db, STRING_LENGTH
from api.storage.models import Image
from api.place.models import Attraction
from api.user.models import User


class Vehicle(BaseDocument):
    name = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)
    icon = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)


class Experience(BaseDocument):
    name = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)
    icon = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)


class ItCategory(BaseDocument):
    name = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)
    icon = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)


class DayAct(BaseEmbeddedDocument):
    time = db.StringField(max_length=STRING_LENGTH['EX_SHORT'], default='00:00')
    description = db.StringField(max_length=STRING_LENGTH['EX_LARGE'])

    attraction = db.ReferenceField(Attraction)
    vehicle = db.ReferenceField(Vehicle, required=True)

    images = db.ListField(db.EmbeddedDocumentField(Image), default=[])

    def output(self, *args, **kwargs):
        if self.attraction:
            attraction = self.attraction.output()
        else:
            attraction = {}

        data = super().output()
        data['attraction'] = attraction
        return data


class ItDay(BaseDocument):
    activities = db.ListField(db.EmbeddedDocumentField(DayAct), default=[])
    itinerary = db.ReferenceField('Itinerary', required=True)


class Itinerary(BaseDocument):
    status = db.IntField(required=True)
    num_of_view = db.IntField(default=0)

    name = db.StringField(max_length=STRING_LENGTH['LONG'])
    description = db.StringField(max_length=STRING_LENGTH['EX_LARGE'])
    start_date = db.FloatField()

    cover_images = db.ListField(db.EmbeddedDocumentField(Image), required=True)

    days = db.ListField(db.ReferenceField(ItDay, reverse_delete_rule=db.PULL), default=[])
    experiences = db.ListField(db.ReferenceField(Experience, reverse_delete_rule=db.DENY), default=[])

    category = db.ReferenceField(ItCategory, reverse_delete_rule=db.DENY)

    def output(self, *args, **kwargs):
        user_data = self._get_user_data(user_id=self.created_by)
        data = super().output(*args, **kwargs)
        data['user'] = user_data
        return data

    def _get_user_data(self, user_id):
        user = User.objects(id=user_id).first()
        showed_fields = ['email', 'id', 'name', 'exp_year', 'address', 'is_tour_guide', 'phone']
        return user.output(includes=showed_fields)
