from api.common.base_models import BaseEmbeddedDocument, BaseDocument, db, STRING_LENGTH


class Country(BaseDocument):
    name = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)


class City(BaseDocument):
    name = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)
    country = db.ReferenceField(Country, required=True)


class Attraction(BaseDocument):
    place_id = db.StringField(max_length=STRING_LENGTH['LONG'], required=True, unique=True)
    name = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)

    latitude = db.FloatField(required=True)
    longitude = db.FloatField(required=True)

    country = db.ReferenceField(Country, required=True)
    city = db.ReferenceField(City)

    def output(self, *args, **kwargs):
        country_name = self.country.name
        if self.city:
            city_name = self.city.name
        else:
            city_name = None

        data = super().output(*args, **kwargs)

        data['city'] = city_name
        data['country'] = country_name

        return data
