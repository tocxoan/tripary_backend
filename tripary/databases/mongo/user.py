from api.common.base_models import db, STRING_LENGTH, BaseDocument, SimpleEmbeddedDocument, BaseEmbeddedDocument
from api.common.methods import get_constant_by_name

from .constants import GENDERS


class Person(object):
    email = db.StringField(max_length=STRING_LENGTH['LONG'])
    name = db.StringField(max_length=STRING_LENGTH['LONG'])
    address = db.StringField(max_length=STRING_LENGTH['LONG'])
    phone = db.StringField(max_length=STRING_LENGTH['EX_SHORT'])
    avatar = db.StringField(max_length=STRING_LENGTH['EX_LONG'])

    gender = db.IntField(default=get_constant_by_name(GENDERS, 'unknown'))

    birthday = db.DictField()


class FacebookInfo(SimpleEmbeddedDocument, Person):
    uid = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)


class User(BaseDocument, Person):
    password = db.StringField(max_length=STRING_LENGTH['EX_SHORT'])

    is_tour_guide = db.BooleanField(default=False)

    exp_year = db.IntField(default=0)

    facebook_info = db.EmbeddedDocumentField(FacebookInfo)

    # role = db.IntField(required=True)
