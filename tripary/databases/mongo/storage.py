from api.common.base_models import db, STRING_LENGTH, BaseEmbeddedDocument


class File(object):
    url = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)
    path = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)
    name = db.StringField(max_length=STRING_LENGTH['LONG'], required=True)
    content_type = db.StringField(max_length=STRING_LENGTH['SHORT'], required=True)
    size = db.IntField(required=True)
    created_by = db.ObjectIdField(required=True)


class Image(BaseEmbeddedDocument, File):
    description = db.StringField(max_length=STRING_LENGTH['LARGE'])
