import re
from datetime import datetime
from marshmallow import fields, ValidationError, Schema
from sanic.request import File
from bson import ObjectId

from .utils import convert_string_to_datetime
from .constants import (DATE_FORMAT, VALID_DATETIME_FORMATS, PAGINATION, PHONE_REGEX,
                        STRING_LENGTH, ITINERARY_STATUSES, VOTE_TYPES)

STRING_LENGTH_VALIDATORS = {
    'EX_SHORT': lambda value: len(value) <= STRING_LENGTH['EX_SHORT'],
    'SHORT': lambda value: len(value) <= STRING_LENGTH['SHORT'],
    'MEDIUM': lambda value: len(value) <= STRING_LENGTH['MEDIUM'],
    'LONG': lambda value: len(value) <= STRING_LENGTH['LONG'],
    'EX_LONG': lambda value: len(value) <= STRING_LENGTH['EX_LONG'],
    'LARGE': lambda value: len(value) <= STRING_LENGTH['LARGE'],
    'EX_LARGE': lambda value: len(value) <= STRING_LENGTH['EX_LARGE']
}


class IdField(fields.Field):
    def _validate(self, value):
        if not isinstance(value, str):
            raise ValidationError('Invalid id.')
        if len(value) != STRING_LENGTH['UUID4']:
            raise ValidationError('Invalid id.')


class BsonIdField(fields.Field):
    def _validate(self, value):
        if isinstance(value, ObjectId):
            return value
        try:
            ObjectId(value)
        except Exception as error:
            raise ValidationError('Invalid object id %s' % error.args)

    def deserialize(self, value, attr=None, data=None):
        result = super().deserialize(value, attr, data)
        if not result:
            return None
        return str(result)

    def serialize(self, attr, obj, accessor=None):
        result = super().serialize(attr, obj, accessor)
        return str(result)


class FileField(fields.Field):
    def _validate(self, value):
        if not isinstance(value, File):
            raise ValidationError('Invalid file.')


class PhoneField(fields.Field):
    def _validate(self, value):
        if not isinstance(value, str) or not re.compile(PHONE_REGEX).match(value):
            raise ValidationError('Invalid phone.')


class StringField(fields.String):
    def deserialize(self, value, attr=None, data=None):
        result = super().deserialize(value, attr, data)
        if result:
            result = result.strip()
        return result


class ListField(fields.List):
    def _validate(self, value):
        if isinstance(value, str):
            value = value.split(',')
        return super()._validate(value)

    def deserialize(self, value, attr=None, data=None):
        if isinstance(value, str):
            value = value.split(',')
        return super().deserialize(value, attr, data)


class DatetimeField(fields.Field):
    def _validate(self, value):
        if not isinstance(value, datetime):
            if isinstance(value, float) or isinstance(value, int):
                try:
                    value = datetime.utcfromtimestamp(value)
                except Exception:
                    raise ValidationError('Invalid datetime!')

            if isinstance(value, str):
                _v = convert_string_to_datetime(value, VALID_DATETIME_FORMATS)
                if not _v:
                    raise ValidationError('Invalid datetime!')
                value = _v
        return super()._validate(value)

    def deserialize(self, value, attr=None, data=None):
        if isinstance(value, (str, int)):
            try:
                value = float(value)
            except ValueError:
                pass

        if isinstance(value, float):
            value = datetime.utcfromtimestamp(value)

        elif isinstance(value, str):
            _v = convert_string_to_datetime(value, VALID_DATETIME_FORMATS)
            if _v:
                value = _v

        return super().deserialize(value, attr, data)

    def serialize(self, attr, obj, accessor=None):
        result = super().serialize(attr, obj, accessor)
        return result.strftime(DATE_FORMAT)


class DateRangeField(fields.Field):
    def _validate(self, value):
        if not isinstance(value, list):
            raise ValidationError('Invalid date range! Must be an array.')

        if len(value) != 2:
            raise ValidationError('Invalid date range! Array must contains 2 items.')

        start, end = value
        if start > end:
            raise ValidationError('End must be greater than start.')

    def deserialize(self, value, attr=None, data=None):
        if isinstance(value, str):
            value = value.split(',')
        for i, item in enumerate(value):
            v = DatetimeField().deserialize(item)
            value[i] = v
        return super().deserialize(value, attr, data)

    def serialize(self, attr, obj, accessor=None):
        date_range = super().serialize(attr, obj, accessor)
        result = []
        for item in date_range:
            result.append(item.strftime(DATE_FORMAT))
        return result


class TimeField(fields.Field):
    def _validate(self, value):
        if not isinstance(value, str) or len(value) > 5:
            raise ValidationError('Invalid time.')
        try:
            hour, minute = value.split(':')
            hour = int(hour)
            minute = int(minute)
        except ValueError:
            raise ValidationError('Invalid time.')

        if hour < 0 or hour > 23 or minute < 0 or minute > 59:
            raise ValidationError('Invalid time.')


class ItineraryStatusField(fields.Field):
    def _validate(self, value):
        if value not in list(map(lambda i: i['id'], ITINERARY_STATUSES)):
            raise ValidationError('Invalid itinerary status.')


class VoteTypeField(fields.Field):
    def _validate(self, value):
        if value not in list(map(lambda i: i['id'], VOTE_TYPES)):
            raise ValidationError('Invalid vote_type.')


class BaseSchema(Schema):
    pass


class BaseInputSchema(BaseSchema):
    pass


class BaseCreatingSchema(BaseInputSchema):
    pass


class BaseUpdatingSchema(BaseInputSchema):
    id = IdField(required=True)


class BaseGettingSchema(BaseInputSchema):
    id = IdField(required=True)


class BaseListingSchema(BaseInputSchema):
    page = fields.Integer(default=PAGINATION['page'], missing=PAGINATION['page'])
    per_page = fields.Integer(default=PAGINATION['per_page'], missing=PAGINATION['per_page'])
    order = fields.String(default='-created_at',
                          missing='-created_at',
                          validate=STRING_LENGTH_VALIDATORS['EX_SHORT'])

    # todo: change all sort logic to this field
    sort_by = ListField(StringField(validate=STRING_LENGTH_VALIDATORS['LONG']),
                        default=['-created_at'],
                        missing=['-created_at'])

    # def load(self, data, many=None, partial=None):
    #     result = super().load(data, many, partial)
    #
    #     if not result.data.get('page'):
    #         result.data['page'] = PAGINATION['page']
    #
    #     if not result.data.get('per_page'):
    #         result.data['per_page'] = PAGINATION['per_page']
    #
    #     if not result.data.get('order'):
    #         result.data['order'] = '-created_at'
    #
    #     return result


class BaseDeletingSchema(BaseInputSchema):
    ids = ListField(IdField(), required=True)


class CoordinatesSchema(BaseSchema):
    lat = fields.Float(required=True)
    lng = fields.Float(required=True)
