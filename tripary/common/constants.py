import os

import config

APP_ROOT_DIR = os.path.dirname(config.__file__)

UPLOAD_DIR = 'static/upload'
if not os.path.exists(os.path.join(APP_ROOT_DIR, UPLOAD_DIR)):
    os.makedirs(os.path.join(APP_ROOT_DIR, UPLOAD_DIR))

TEMPLATE_DIR = os.path.join(APP_ROOT_DIR, 'static/templates')
if not os.path.exists(TEMPLATE_DIR):
    os.makedirs(TEMPLATE_DIR)


DATE_FORMAT = '%d/%m/%Y'
DATETIME_FORMAT = '%d/%m/%Y %H:%M:%S'

VALID_DATETIME_FORMATS = [
    DATE_FORMAT,
    DATETIME_FORMAT
]

JWT_TOKEN_EXPIRE = 7200  # second

DEFAULT_METHODS_PARAM_LOCATION = {
    'post': 'json',
    'patch': 'json',
    'put': 'json',
    'get': 'raw_args',
    'delete': 'raw_args',
}

PAGINATION = {
    'page': 1,
    'per_page': 50
}

STRING_LENGTH = {
    'UUID4': 36,
    'EX_SHORT': 30,
    'SHORT': 50,
    'MEDIUM': 200,
    'LONG': 500,
    'EX_LONG': 1000,
    'LARGE': 3000,
    'EX_LARGE': 10000
}

PHONE_REGEX = '^(\+8[0-9]{9,12})$|^(0[0-9]{6,15})$'

ITINERARY_STATUSES = [
    dict(id='public', name='Công khai', icon='public'),
    dict(id='share', name='User hoặc nhóm', icon='group'),
    dict(id='private', name='Chỉ mình tôi', icon='lock'),
]

USER_STATUSES = [
    dict(id='active', name='Hoạt động'),
    dict(id='locked', name='Khóa')
]

FILE_TYPES = {
    'image': ['jpg', 'jpeg', 'png'],
    'attachment': ['pdf', 'xls', 'xlsx', 'docx', 'doc']
}

VOTE_TYPES = [
    dict(id='up'),
    dict(id='down')
]

DEFAULT_ITINERARY_META = {
    'stay_suggestion': True,
    'cuisine_suggestion': True,
    'travel_suggestion': True,
    'activity_suggestion': True,
    'suggestion_noti_dialog_disabled': False
}

MAXIMUM_IMAGE_WIDTH = 3000
MAX_IMAGE_COUNT = 15
MAX_ATTACHMENT_COUNT = 5
NOTIFICATION_TYPE_INTERVAL = 300  # seconds
