import jwt
import logging
import secrets
from collections import OrderedDict
from datetime import datetime, timedelta
from cryptography.fernet import Fernet

from tripary.common.constants import JWT_TOKEN_EXPIRE

log = logging.getLogger('two_fa')


def get_now(local=False, timestamp=False):
    if local:
        now = datetime.now()
    else:
        now = datetime.utcnow()

    if timestamp:
        return now.timestamp()

    return now


def get_record_by_id(model, id, get_query=False):
    query = model.objects(id=id, deleted=False)
    if not query.count(True):
        return None

    if get_query:
        return query
    return query.first()


def encrypt(string, secret_key):
    if not isinstance(string, bytes):
        string = string.encode()
    fernet = Fernet(key=secret_key)
    return fernet.encrypt(string).decode()


def decrypt(token, secret_key):
    if not isinstance(token, bytes):
        token = token.encode()
    fernet = Fernet(key=secret_key)
    return fernet.decrypt(token).decode()


def make_jwt_token(secret_key, **kwargs):
    now = datetime.now()
    expire = now + timedelta(seconds=JWT_TOKEN_EXPIRE)
    payload = OrderedDict(
        exp=expire,
        **kwargs
    )
    return jwt.encode(payload, secret_key, algorithm='HS256').decode()


def decode_jwt_token(token, secret_key, verify_exp=False):
    try:
        token_data = jwt.decode(token, secret_key, options={'verify_exp': verify_exp})

    except Exception as e:
        return None
    return token_data


def generate_code(nbytes=None):
    if not nbytes:
        nbytes = 2
    return secrets.token_hex(nbytes)


def paginate(query, page, per_page):
    return query.offset((page - 1) * per_page).limit(per_page).all()


def back_to_first_day_of_month(date):
    return date.replace(day=1, hour=0, minute=0,
                        second=0, microsecond=0)


def back_to_the_beginning_of_day(date):
    return date.replace(hour=0, minute=0,
                        second=0, microsecond=0)


def get_to_the_end_of_day(date):
    result = date + timedelta(days=1)
    result = back_to_the_beginning_of_day(result) - timedelta(milliseconds=1)
    return result


def convert_string_to_datetime(string, formats):
    value = None
    for datetime_format in formats:
        try:
            value = datetime.strptime(string, datetime_format)
            break
        except ValueError:
            continue

    return value


def sort_query(string, table, query):
    # get sort_field and desc
    if string.startswith('-'):
        field_name = string[1:len(string)]
        descending = True
    else:
        descending = False
        if string.startswith('+'):
            field_name = string[1:len(string)]
        else:
            field_name = string
    # sorting
    sort_by = getattr(table, field_name, None)
    if sort_by:
        if descending:
            sort_by = sort_by.desc()
        query = query.order_by(sort_by)
    return query
