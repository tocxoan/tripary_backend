import os
from slugify import slugify
from google.cloud import storage
from PIL import Image as PillowImage

from tripary.common.constants import MAXIMUM_IMAGE_WIDTH, FILE_TYPES, APP_ROOT_DIR
from tripary.common.utils import get_now

from .errors import *


class Client(object):

    def __init__(self, config):
        self.bucket_name = config['bucket_name']
        self.credentials = config['credentials']
        self.env = config['env']

        self.bucket = self._get_bucket()

        self.file_type_handlers = {
            'image': self._handle_image,
            'attachment': self._handle_attachment
        }

    def upload(self, file_type, file_name, file_path, path=None):

        handled_file_name = self._handle_file_name(file_name)

        try:
            file_type_handler = self.file_type_handlers[file_type]
        except KeyError:
            raise FileTypeNotSupported

        self._validate_file_type(handled_file_name, file_type)

        blob_path = self._make_blob_path(path=path,
                                         file_name=handled_file_name)

        return file_type_handler(file_path=file_path,
                                 file_name=handled_file_name,
                                 blob_path=blob_path)

    def remove(self, path, multi=False):
        if isinstance(path, str):
            full_path = path
        else:
            full_path = os.path.join(self.env, '/'.join(path))

        if multi:
            blobs = self.bucket.list_blobs(prefix=full_path)
            self.bucket.delete_blobs(blobs=list(blobs))
            return

        blob = self.bucket.get_blob(full_path)
        if blob:
            blob.delete()
        return

    def _handle_file_name(self, file_name):
        split_name = file_name.split('.')
        name = '.'.join(split_name[:len(split_name) - 1])
        ext = split_name[len(split_name) - 1]

        slug = '%s.%s' % (slugify(name), ext)

        timestamp = str(int(get_now().timestamp()))

        return '%s_%s' % (timestamp, slug)

    def _handle_image(self, file_name, blob_path, file_path):
        pil_resized_image = self._resize_image(file_path=file_path)
        tmp_path = self._make_tmp_path(file_name)

        pil_resized_image.save(tmp_path)
        try:
            image_url = self._upload_to_gcloud(tmp_path=tmp_path,
                                               blob_path=blob_path)
        except Exception as e:
            raise e

        size = os.path.getsize(tmp_path)
        os.remove(tmp_path)

        file_data = dict(
            url=image_url,
            path=blob_path,
            size=size
        )
        return file_data

    def _handle_attachment(self, file_name, blob_path, file_path):
        try:
            file_url = self._upload_to_gcloud(tmp_path=file_path,
                                              blob_path=blob_path)
        except Exception as e:
            raise e

        size = os.path.getsize(file_path)

        file_data = dict(
            url=file_url,
            path=blob_path,
            size=size
        )
        return file_data

    def _make_tmp_path(self, file_name):
        tmp = os.path.join('tmp', file_name)
        return os.path.join(APP_ROOT_DIR, tmp)

    def _validate_file_type(self, file_name, file_type):
        file_ext = self._get_file_ext(file_name)
        if file_ext not in FILE_TYPES[file_type]:
            raise ContentTypeNotSupported

    def _get_storage_client(self):
        # Google app engine env variables
        credential_path = os.path.join(APP_ROOT_DIR, self.credentials)
        gcloud_storage_client = storage.Client.from_service_account_json(credential_path)
        return gcloud_storage_client

    def _get_bucket(self):
        client = self._get_storage_client()
        return client.get_bucket(self.bucket_name)

    def _make_blob_path(self, file_name, path=None):
        if not path:
            path = []
        full_path = os.path.join(self.env, '/'.join(path))
        return os.path.join(full_path, file_name)

    def _get_file_ext(self, file_name):
        split_name = file_name.split('.')
        return split_name[len(split_name) - 1].lower()

    def _upload_to_gcloud(self, tmp_path, blob_path):
        bucket = self._get_bucket()
        blob = bucket.blob(blob_path)
        blob.upload_from_filename(tmp_path)
        blob.make_public()
        return blob.public_url

    def _resize_image(self, file_path, width=None, ratio=None):

        pil_image = PillowImage.open(file_path)

        if not ratio:
            if not width:
                width = MAXIMUM_IMAGE_WIDTH
            ratio = width / pil_image.width

        # only resize if image's width greater than maximum width
        if ratio < 1:
            return pil_image.resize((int(pil_image.width * ratio),
                                     int(pil_image.height * ratio)),
                                    PillowImage.ANTIALIAS)
        return pil_image
