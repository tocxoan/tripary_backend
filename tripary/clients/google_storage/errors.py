class FileTypeNotSupported(Exception):
    pass


class ContentTypeNotSupported(Exception):
    pass
