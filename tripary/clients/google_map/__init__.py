import requests

from .constants import PLACE_FIELDS


class Client(object):
    def __init__(self, config):
        self.API_KEY = config['api_key']

    def get_place_data(self, place_id):
        endpoint = 'https://maps.googleapis.com/maps/api/place/details/json'
        params = dict(
            placeid=place_id,
            key=self.API_KEY,
            language='vi',
            fields=','.join(PLACE_FIELDS)
        )
        try:
            response = requests.get(url=endpoint, params=params)
        except Exception as e:
            raise e

        if response.status_code != 200:
            raise Exception('Could not get place data')

        try:
            res_data = response.json()
        except Exception as e:
            raise e

        if res_data['status'] != 'OK':
            raise Exception('%s' % res_data['error_message'])

        return res_data['result']
