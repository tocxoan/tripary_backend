PLACE_FIELDS = [
    'name', 'formatted_address', 'place_id', 'url', 'photos', 'types',
    'geometry', 'address_components', 'website', 'international_phone_number'
]
