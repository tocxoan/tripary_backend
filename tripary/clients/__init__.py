from config import ClientsConfig

from .google_storage import Client as GoogleStorageClient
from .google_map import Client as GoogleMapClient

google_storage_client = GoogleStorageClient(ClientsConfig.GOOGLE_STORAGE)
google_map_client = GoogleMapClient(ClientsConfig.GOOGLE_MAP)
