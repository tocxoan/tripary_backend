from envelopes import Envelope, GMailSMTP

from .errors import *


class Client(object):

    def __init__(self, config):
        self.config = config
        self.account = config['account']
        self.password = config['password']
        self.port = config['port']
        self.smtp = config['smtp']

    def send(self, to_addr, subject, body, cc=None):

        params = dict(
            from_addr='Tripary <%s>' % self.account,
            to_addr=to_addr,
            subject=self._handle_subject(subject),
            html_body=body
        )

        if cc:
            params['cc_addr'] = cc

        envelope = Envelope(**params)

        try:
            gmail = GMailSMTP(self.account, self.password)
            response = gmail.send(envelope)

        except Exception:
            raise

        return response

    def _handle_subject(self, subject):
        result = subject
        return result
