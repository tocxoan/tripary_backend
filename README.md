# Tripary backend

## Requirements
- Ubuntu 18.04
- Python 3.6+

## Configuration
Use config.py.example to create config.py file
Create gcloud-credentials.json in the same directory as config.py

## Setup
`pip3 install pipenv`

`pipenv install`

`alembic upgrade head`

`pipenv run python run.py init-db`

## Run
##### Public api
`pipenv run python run.py api --enable-uwsgi=true --api-file=public_api.py --host=127.0.0.1 --port=3001 --processes=2 --threads=5`

##### Mail gun consumer
`pipenv run python run.py consumer mail`

## DOC
TODO
